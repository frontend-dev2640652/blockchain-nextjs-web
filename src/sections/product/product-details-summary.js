import PropTypes from 'prop-types';
import { useForm } from 'react-hook-form';
import { useState, useEffect, useCallback, useMemo } from 'react';

import Box from '@mui/material/Box';
import Link from '@mui/material/Link';
import Stack from '@mui/material/Stack';
import Rating from '@mui/material/Rating';
import Button from '@mui/material/Button';
import Divider from '@mui/material/Divider';
import MenuItem from '@mui/material/MenuItem';
import Typography from '@mui/material/Typography';
import { formHelperTextClasses } from '@mui/material/FormHelperText';

import { paths } from 'src/routes/paths';
import { useRouter } from 'src/routes/hooks';

import { useMockedUser } from 'src/hooks/use-mocked-user';

import axios, { endpoints } from 'src/utils/axios';
import { fCurrency, fShortenNumber } from 'src/utils/format-number';

import Iconify from 'src/components/iconify';
import { useSnackbar } from 'src/components/snackbar';
import FormProvider, { RHFSelect } from 'src/components/hook-form';

import IncrementerButton from './common/incrementer-button';


// ----------------------------------------------------------------------

export default function ProductDetailsSummary({
  items,
  product,
  onAddCart,
  onGotoStep,
  disabledActions,
  onAddToCartSuccess,
  ...other
}) {
  const router = useRouter();

  const { user } = useMockedUser();

  const { enqueueSnackbar } = useSnackbar();

  // State để lưu trữ thông tin của giỏ hàng
  const [cart, setCart] = useState([]);

  const {
    id,
    productName,
    price,
    imgUrl,
    // colors,
    // newLabel,
    unitOfStock,
    // priceSale,
    // saleLabel,
    avgReview,
    // totalReviews,
    //  inventoryType,
    description,
  } = product;

  const existProduct = !!items?.length && items.map((item) => item.id).includes(id);

  // const isMaxQuantity =
  //   !!items?.length
  //   &&
  //   items.filter((item) => item.id === id).map((item) => item.quantity)[0] >= unitOfStock;


// Inside your component
const defaultValues = useMemo(() => ({
  id,
  productName,
  imgUrl,
  unitOfStock,
  price,
  quantity: unitOfStock < 1 ? 0 : 1,
}), [id, productName, imgUrl, unitOfStock, price]);

  const methods = useForm({
    defaultValues,
  });

  const { reset, watch, setValue, handleSubmit } = methods;

  const values = watch();

const fetchCart = useCallback(async () => {
  const url = `${endpoints.product.list_cart}?userId=${encodeURIComponent(user?.id)}`;
  try {
    const { data } = await axios.get(url , {
      headers: {
        Authorization: `Bearer ${user?.accessToken}`,
        'Content-Type': 'application/json',
      }
    });

    if (data?.status && data?.status === 'OK') {
      setCart(data?.data?.product);
      enqueueSnackbar(data?.message, { variant: 'success' });
    } else {
      enqueueSnackbar(data?.message, { variant: 'error' });
    }
  } catch (error) {
    console.error('Failed to fetch cart', error);
    enqueueSnackbar('Error fetching cart', { variant: 'error' });
  }
}, [user?.id, user?.accessToken, enqueueSnackbar]);

useEffect(() => {
  fetchCart();
}, [fetchCart]);




const handleAddToCart = useCallback(async () => {
  const body = {
    productId: product?.id,
    userId: user?.id,
    quantity: values?.quantity,
  };

  try {
    const response = await axios.post('/api/auth/add-product-to-cart', body, {
      headers: {
        'Content-Type': 'application/json',
        // Thêm Authorization header nếu API yêu cầu
        Authorization: `Bearer ${user?.accessToken}`, 
      },
    });
    
    if (response?.data?.status === 'OK') {
      enqueueSnackbar('Product added to cart!', { variant: 'success' });
      setCart(response?.data?.product);
      if (onAddToCartSuccess) {
        onAddToCartSuccess();
      }
      fetchCart();
    } else {
      enqueueSnackbar('Failed to add product to cart', { variant: 'error' });
    }
  } catch (error) {
    console.error('Error adding product to cart:', error);
    enqueueSnackbar('Error adding product to cart', { variant: 'error' });
  }
}, [product?.id, user?.id, user?.accessToken, values?.quantity, enqueueSnackbar, onAddToCartSuccess, fetchCart]);




  useEffect(() => {
    if (product) {
      reset(defaultValues);
    }
  }, [defaultValues, product, reset]);

  const onSubmit = handleSubmit(async (data) => {
    try {
      if (!existProduct) {
        onAddCart?.({
          ...data,
          // colors: [values.colors],
          subTotal: data.price * data.quantity,
        });
      }
      onGotoStep?.(0);
      router.push(paths.product.checkout);
    } catch (error) {
      console.error(error);
    }
  });

  const handleAddCart = useCallback(() => {
    try {
      onAddCart?.({
        ...values,
        // colors: [values.colors],
        subTotal: values.price * values.quantity,
      });
    } catch (error) {
      console.error(error);
    }
  }, [onAddCart, values]);

  const renderPrice = (
    <Box sx={{ typography: 'h5' }}>
      {/* //   {priceSale && ( */}
      {/* //     <Box */}
      {/* //       component="span" */}
      {/* //       sx={{ */}
      {/* //         color: 'text.disabled', */}
      {/* //         textDecoration: 'line-through', */}
      {/* //         mr: 0.5, */}
      {/* //       }} */}
      {/* //     > */}
      {/* //       {fCurrency(priceSale)} */}
      {/* //     </Box> */}
      {/* //    )} */}

      {fCurrency(price)}
    </Box>
  );

  const renderShare = (
    <Stack direction="row" spacing={3} justifyContent="center">
      <Link
        variant="subtitle2"
        sx={{
          color: 'text.secondary',
          display: 'inline-flex',
          alignItems: 'center',
        }}
      >
        <Iconify icon="mingcute:add-line" width={16} sx={{ mr: 1 }} />
        Compare
      </Link>

      <Link
        variant="subtitle2"
        sx={{
          color: 'text.secondary',
          display: 'inline-flex',
          alignItems: 'center',
        }}
      >
        <Iconify icon="solar:heart-bold" width={16} sx={{ mr: 1 }} />
        Favorite
      </Link>

      <Link
        variant="subtitle2"
        sx={{
          color: 'text.secondary',
          display: 'inline-flex',
          alignItems: 'center',
        }}
      >
        <Iconify icon="solar:share-bold" width={16} sx={{ mr: 1 }} />
        Share
      </Link>
    </Stack>
  );

  // const renderColorOptions = (
  //   <Stack direction="row">
  //     <Typography variant="subtitle2" sx={{ flexGrow: 1 }}>
  //       Color
  //     </Typography>

  //     {/* <Controller */}
  //     {/*   name="colors" */}
  //     {/*   control={control} */}
  //     {/*   render={({ field }) => ( */}
  //     {/*     <ColorPicker */}
  //     {/*       colors={colors} */}
  //     {/*       selected={field.value} */}
  //     {/*       onSelectColor={(color) => field.onChange(color)} */}
  //     {/*       limit={4} */}
  //     {/*     /> */}
  //     {/*   )} */}
  //     {/* /> */}
  //   </Stack>
  // );

  const renderSizeOptions = (
    <Stack direction="row">
      <Typography variant="subtitle2" sx={{ flexGrow: 1 }}>
        Size
      </Typography>

      <RHFSelect
        name="size"
        size="small"
        // helperText={
        //   <Link underline="always" color="textPrimary">
        //     Size Chart
        //   </Link>
        // }
        sx={{
          maxWidth: 88,
          [`& .${formHelperTextClasses.root}`]: {
            mx: 0,
            mt: 1,
            textAlign: 'right',
          },
        }}
      >
        {/* {size.map((sizes) => (
          <MenuItem key={sizes} value={sizes}>
            {sizes}
          </MenuItem>
        ))} */}
      </RHFSelect>
    </Stack>
  );

  const renderQuantity = (
    <Stack direction="row">
      <Typography variant="subtitle2" sx={{ flexGrow: 1 }}>
        Quantity
      </Typography>

      <Stack spacing={1}>
        <IncrementerButton
          name="quantity"
          quantity={values.quantity}
          disabledDecrease={values.quantity <= 1}
          disabledIncrease={values.quantity >= unitOfStock}
          onIncrease={() => setValue('quantity', values.quantity + 1)}
          onDecrease={() => setValue('quantity', values.quantity - 1)}
        />

        <Typography variant="caption" component="div" sx={{ textAlign: 'right' }}>
          Available: {unitOfStock}
        </Typography>
      </Stack>
    </Stack>
  );

  const isOutOfStock = unitOfStock < 1;

  const renderActions = (
    <Stack direction="row" spacing={2}>
      <Button
        fullWidth
        disabled={isOutOfStock || disabledActions}
        size="large"
        color="warning"
        variant="contained"
        startIcon={<Iconify icon="solar:cart-plus-bold" width={24} />}
        onClick={handleAddToCart}
        sx={{ whiteSpace: 'nowrap' }}
      >
        Add to Cart
      </Button>

      <Button
        fullWidth
        size="large"
        type="submit"
        variant="contained"
        disabled={isOutOfStock || disabledActions}
      >
        Buy Now
      </Button>
    </Stack>
  );

  const renderSubDescription = (
    <Typography variant="body2" sx={{ color: 'text.secondary' }}>
      {description}
    </Typography>
  );

  const renderRating = (
    <Stack
      direction="row"
      alignItems="center"
      sx={{
        color: 'text.disabled',
        typography: 'body2',
      }}
    >
      <Rating size="small" value={avgReview} precision={0.1} readOnly sx={{ mr: 1 }} />
      {`(${fShortenNumber(avgReview)} reviews)`}
    </Stack>
  );

  // const renderLabels = (newLabel.enabled || saleLabel.enabled) && (
  //   <Stack direction="row" alignItems="center" spacing={1}>
  //     {newLabel.enabled && <Label color="info">{newLabel.content}</Label>}
  //     {saleLabel.enabled && <Label color="error">{saleLabel.content}</Label>}
  //   </Stack>
  // );

  // const inventoryStatusLabel = unitOfStock > 5 ? 'In Stock' : unitOfStock > 0 ? 'Low Stock' : 'Out of Stock';

  let inventoryStatusLabel;
  if (unitOfStock > 5) {
    inventoryStatusLabel = 'In Stock';
  } else if (unitOfStock > 0) {
    inventoryStatusLabel = 'Low Stock';
  } else {
    inventoryStatusLabel = 'Out of Stock';
  }
  const inventoryType = inventoryStatusLabel.toLowerCase();
  const renderInventoryType = (
    <Box
      component="span"
      sx={{
        typography: 'overline',
        color:
          (inventoryType === 'out of stock' && 'error.main') ||
          (inventoryType === 'low stock' && 'warning.main') ||
          'success.main',
      }}
    >
      {inventoryStatusLabel}
    </Box>
  );

  return (
    <FormProvider methods={methods} onSubmit={onSubmit}>
      <Stack spacing={3} sx={{ pt: 3 }} {...other}>
        <Stack spacing={2} alignItems="flex-start">
          {/* {renderLabels} */}

          {renderInventoryType}

          <Typography variant="h5">{productName}</Typography>

          {renderRating}

          {renderPrice}

          {renderSubDescription}
        </Stack>

        <Divider sx={{ borderStyle: 'dashed' }} />

        {/* {renderColorOptions} */}
        {/* 
        {renderSizeOptions} */}

        {renderQuantity}

        <Divider sx={{ borderStyle: 'dashed' }} />

        {renderActions}

        {/* {renderShare} */}
      </Stack>
    </FormProvider>
  );
}

ProductDetailsSummary.propTypes = {
  items: PropTypes.array,
  disabledActions: PropTypes.bool,
  onAddCart: PropTypes.func,
  onGotoStep: PropTypes.func,
  product: PropTypes.object,
  onAddToCartSuccess: PropTypes.func,
};
