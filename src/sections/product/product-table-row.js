import PropTypes from 'prop-types';

import Box from '@mui/material/Box';
import Link from '@mui/material/Link';
import Stack from '@mui/material/Stack';
import Avatar from '@mui/material/Avatar';
import ListItemText from '@mui/material/ListItemText';
import LinearProgress from '@mui/material/LinearProgress';

import { fCurrency } from 'src/utils/format-number';
import { fTime, fDate } from 'src/utils/format-time';

import Label from 'src/components/label';

// ----------------------------------------------------------------------

export function RenderCellPrice({ params }) {
  return <>{fCurrency(params.row.price)}</>;
}

RenderCellPrice.propTypes = {
  params: PropTypes.shape({
    row: PropTypes.object,
  }),
};

export function RenderCellPublish({ params }) {
  return (
    <Label variant="soft" color={(params.row.publish === 'published' && 'info') || 'default'}>
      {params.row.publish}
    </Label>
  );
}

RenderCellPublish.propTypes = {
  params: PropTypes.shape({
    row: PropTypes.object,
  }),
};

export function RenderCellCreatedAt({ params }) {
  return (
    <ListItemText
      primary={fDate(params.row.createdAt)}
      secondary={fTime(params.row.createdAt)}
      primaryTypographyProps={{ typography: 'body2', noWrap: true }}
      secondaryTypographyProps={{
        mt: 0.5,
        component: 'span',
        typography: 'caption',
      }}
    />
  );
}

RenderCellCreatedAt.propTypes = {
  params: PropTypes.shape({
    row: PropTypes.object,
  }),
};

export function RenderCellStock({ params }) {
  return (
    <Stack sx={{ typography: 'caption', color: 'text.secondary' }}>
      <LinearProgress
        value={(params.row.available * 100) / params.row.quantity}
        variant="determinate"
        color={
          (params.row.unitOfStock === 'out of stock' && 'error') ||
          (params.row.unitOfStock === 'low stock' && 'warning') ||
          'success'
        }
        sx={{ mb: 1, height: 6, maxWidth: 80 }}
      />
      {!!params.row.available && params.row.available} {params.row.unitOfStock}
    </Stack>
  );
}

RenderCellStock.propTypes = {
  params: PropTypes.shape({
    row: PropTypes.object,
  }),
};
export function RenderCellProductName({ params }) {
  return (
    <Stack direction="column" alignItems="start" sx={{ py: 2, width: 1 }}>
      <ListItemText
        disableTypography
        primary={
          <Box component="div" sx={{ typography: 'body1', color: 'text.primary' }}>
            {params.row.productName}
          </Box>
        }
      />
    </Stack>
  );
}
export function RenderCellProductImage({ params }) {
  const coverUrl = `/assets/images/${params.row.imgUrl}`;
  return (
    <Stack direction="row" alignItems="center" sx={{ py: 2, width: 1 }}>
      <img
        alt={params.row.name}
        src={coverUrl}
        style={{ width: 64, height: 64, marginRight: 2, borderRadius: '50%' }} // Thêm border-radius để làm tròn ảnh
      />

      {/* <ListItemText
        disableTypography
        primary={
          <Link
            noWrap
            color="inherit"
            variant="subtitle2"
            onClick={params.row.onViewRow}
            sx={{ cursor: 'pointer' }}
          >
            {params.row.name}
          </Link>
        }
        secondary={
          <Box component="div" sx={{ typography: 'body2', color: 'text.disabled' }}>
            {params.row.productName}
          </Box>
        }
        sx={{ display: 'flex', flexDirection: 'column' }}
      /> */}
    </Stack>
  );
}
export function RenderCellDescription({ params }) {
  return (
    <Stack direction="column" alignItems="start" sx={{ py: 2, width: 1 }}>
      <ListItemText
        disableTypography
        primary={
          <Box component="div" sx={{ typography: 'body1', color: 'text.primary' }}>
            {params.row.description}
          </Box>
        }
      />
    </Stack>
  );
}
RenderCellDescription.propTypes = {
  params: PropTypes.shape({
    row: PropTypes.object,
  }),
};
RenderCellProductImage.propTypes = {
  params: PropTypes.shape({
    row: PropTypes.object,
  }),
};
RenderCellProductName.propTypes = {
  params: PropTypes.shape({
    row: PropTypes.object,
  }),
};
