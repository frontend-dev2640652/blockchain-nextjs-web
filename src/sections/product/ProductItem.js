import PropTypes from 'prop-types';
import { useForm } from 'react-hook-form';

import Fab from '@mui/material/Fab';
import Box from '@mui/material/Box';
import Link from '@mui/material/Link';
import Card from '@mui/material/Card';
import Stack from '@mui/material/Stack';
import Tooltip from '@mui/material/Tooltip';
import MenuItem from '@mui/material/MenuItem';
import Typography from '@mui/material/Typography';
import { formHelperTextClasses } from '@mui/material/FormHelperText';

import { paths } from 'src/routes/paths';
import { RouterLink } from 'src/routes/components';

import { fCurrency } from 'src/utils/format-number';

import Image from 'src/components/image';
import Iconify from 'src/components/iconify';
import FormProvider, { RHFSelect } from 'src/components/hook-form';

import { useCheckoutContext } from '../checkout/context';

// ----------------------------------------------------------------------

export default function ProductItem({ product }) {
  const { onAddToCart } = useCheckoutContext();

  // Cập nhật cách trích xuất dữ liệu từ prop `product` để phản ánh cấu trúc mới
  const { id, productName: name, imgUrl, price, unitOfStock: available, size } = product;

  const coverUrl = `/assets/images/${imgUrl}`;

  console.log(coverUrl);

  const linkTo = paths.product.details(id);

  const handleAddCart = async () => {
    const newProduct = {
      id,
      name,
      coverUrl,
      available,
      price,
      // Loại bỏ tham chiếu đến colors và sizes vì API không trả về thông tin này
      sizes: product.size,
      quantity: available < 1 ? 0 : 1,
    };
    try {
      onAddToCart(newProduct);
    } catch (error) {
      console.error(error);
    }
  };

  const methods = useForm({
    handleAddCart,
  });

  // Bỏ qua phần renderLabels vì không có thông tin newLabel và saleLabel từ API

  const renderImg = (
    <Box sx={{ position: 'relative', p: 1 }}>
      {!!available && (
        <Fab
          color="warning"
          size="medium"
          className="add-cart-btn"
          onClick={handleAddCart}
          sx={{
            right: 16,
            bottom: 16,
            zIndex: 9,
            opacity: 0,
            position: 'absolute',
            transition: (theme) =>
              theme.transitions.create('all', {
                easing: theme.transitions.easing.easeInOut,
                duration: theme.transitions.duration.shorter,
              }),
          }}
        >
          <Iconify icon="solar:cart-plus-bold" width={24} />
        </Fab>
      )}

      <Tooltip title={!available && 'Out of stock'} placement="bottom-end">
        <Image
          alt={name}
          src={coverUrl}
          ratio="1/1"
          sx={{
            borderRadius: 1.5,
            ...(!available && {
              opacity: 0.48,
              filter: 'grayscale(1)',
            }),
          }}
        />
      </Tooltip>
    </Box>
  );

  // const renderSizeOptions = (
  //   <Stack direction="row">
  //     <Typography variant="subtitle2" sx={{ flexGrow: 1 }}>
  //       Size
  //     </Typography>

  //     <RHFSelect
  //       name="size"
  //       size="small"
  //       // helperText={
  //       //   <Link underline="always" color="textPrimary">
  //       //     Size Chart
  //       //   </Link>
  //       // }
  //       sx={{
  //         marginLeft: 2,
  //         maxWidth: 88,
  //         [`& .${formHelperTextClasses.root}`]: {
  //           mx: 0,
  //           mt: 1,
  //           textAlign: 'right',
  //         },
  //       }}
  //     >
  //       {size.map((sizes) => (
  //         <MenuItem key={sizes} value={sizes}>
  //           {sizes}
  //         </MenuItem>
  //       ))}
  //     </RHFSelect>
  //   </Stack>
  // );

  const renderContent = (
    <FormProvider methods={methods}>
      <Stack spacing={2.5} sx={{ p: 3, pt: 2 }}>
        <Link component={RouterLink} href={linkTo} color="inherit" variant="subtitle2" noWrap>
          {name}
        </Link>

        <Stack direction="row" alignItems="center" justifyContent="space-between">
          {/* {renderSizeOptions} */}
          <Stack direction="row" spacing={0.5} sx={{ typography: 'subtitle1' }}>
            {/* Bỏ qua priceSale vì không có thông tin này từ API */}
            <Box component="span">{fCurrency(price)}</Box>
          </Stack>
        </Stack>
      </Stack>
    </FormProvider>
  );

  return (
    <Card
      sx={{
        '&:hover .add-cart-btn': {
          opacity: 1,
        },
      }}
    >
      {/* Bỏ qua renderLabels vì không có thông tin newLabel và saleLabel từ API */}

      {renderImg}

      {renderContent}
    </Card>
  );
}

ProductItem.propTypes = {
  product: PropTypes.object,
};
