import * as Yup from 'yup';
import PropTypes from 'prop-types';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { useState, useCallback, useEffect } from 'react';

import Button from '@mui/material/Button';
import Grid from '@mui/material/Unstable_Grid2';
import LoadingButton from '@mui/lab/LoadingButton';

import { useMockedUser } from 'src/hooks/use-mocked-user';

import axios, { endpoints } from 'src/utils/axios';

import Iconify from 'src/components/iconify';
import FormProvider from 'src/components/hook-form';
import { useSnackbar } from 'src/components/snackbar';

import { useCheckoutContext } from './context';
import CheckoutSummary from './checkout-summary';
import CheckoutDelivery from './checkout-delivery';
import ModernVerifyView from './verification-form';
// import CheckoutBillingInfo from './checkout-billing-info';
import CheckoutPaymentMethods from './checkout-payment-methods';
// ----------------------------------------------------------------------

const DELIVERY_OPTIONS = [
  {
    value: 0,
    label: 'Keep Gold in Store',
    description: 'Your gold is protected and keep in the store until you comes back!',
  },
  {
    value: 1,
    label: 'Go to Store',
    description: 'This option allows you to go to our store now!',
  },
  // {
  //   value: 20,
  //   label: 'Express',
  //   description: '2-3 Days delivery',
  // },
];

const PAYMENT_OPTIONS = [
  {
    value: 'paypal',
    label: 'Pay with Paypal',
    description: 'You will be redirected to PayPal website to complete your purchase securely.',
  },
  {
    value: 'credit',
    label: 'Credit / Debit Card',
    description: 'We support Mastercard, Visa, Discover and Stripe.',
  },
  {
    value: 'cash',
    label: 'Cash',
    description: 'Pay with cash when your order is delivered.',
  },
];

const CARDS_OPTIONS = [
  { value: 'ViSa1', label: '**** **** **** 1212 - Jimmy Holland' },
  { value: 'ViSa2', label: '**** **** **** 2424 - Shawn Stokes' },
  { value: 'MasterCard', label: '**** **** **** 4545 - Cole Armstrong' },
];



CheckoutPayment.propTypes = {
  onNextStep: PropTypes.func,
  onVerifySuccess: PropTypes.func,
};

export default function CheckoutPayment({ onNextStep, onVerifySuccess }) {

  const [isLoading, setIsLoading] = useState(false);

  const checkout = useCheckoutContext();

  const { user } = useMockedUser();

  const PaymentSchema = Yup.object().shape({
    payment: Yup.string().required('Payment is required'),
  });

  const defaultValues = {
    delivery: checkout.shipping,
    payment: '',
  };

  const methods = useForm({
    resolver: yupResolver(PaymentSchema),
    defaultValues,
  });

  const { handleSubmit, setValue, watch } = methods;

  // const onSubmit = handleSubmit(async (data) => {
  //   try {
  //     checkout.onNextStep();
  //     checkout.onReset();
  //     console.info('DATA', data);
  //   } catch (error) {
  //     console.error(error);
  //   }
  // });

  const { enqueueSnackbar } = useSnackbar();


    const [orderTotal, setOrderTotal] = useState(0);

    const [listCartIdNew, setListCartId] = useState([]);

    const [isConsignmentUser, setIsConsignment] = useState(false);

    const delivery = watch('delivery');

    const [openDialog, setOpenDialog] = useState(false);

    const [orderId, setOrderId] = useState(null);


  const handleCloseDialog = () => {
    setOpenDialog(false);
  };


    const fetchCart = useCallback(async () => {
      const url = `${endpoints.product.list_cart}?userId=${encodeURIComponent(user?.id)}`;
      try {
        const { data } = await axios.get(url, {
          headers: {
            Authorization: `Bearer ${user?.accessToken}`,
            'Content-Type': 'application/json',
          },
        });
    
        if (data?.status === 'OK' || data.data?.status === 'BAD_REQUEST' && Array.isArray(data?.data)) {
          
          const cartItems = data.data;
          const listCart = cartItems.map(item => item.id);
          setListCartId(listCart);
          
          const newOrderTotal = data.data.reduce(
            (total, item) => total + item.price * item.quantity,
            0
        );
        setOrderTotal(newOrderTotal);
        } 
      } catch (error) {
        console.error('Failed to fetch cart', error);
        enqueueSnackbar('Error fetching cart', { variant: 'error' });
      }
    }, [user?.id, user?.accessToken, enqueueSnackbar]);
  
    useEffect(() => {
      fetchCart();
    }, [fetchCart]);


    useEffect(() => {
      if (delivery === DELIVERY_OPTIONS[0].value) {
        setIsConsignment(true);
      } else if (delivery === DELIVERY_OPTIONS[1].value) {
        setIsConsignment(false);
      }
    }, [delivery]);


    const handleCheckout = async () => {
      setIsLoading(true);
      try {
        // Lưu ý: Điều chỉnh API endpoint và body data cho phù hợp với API của bạn
        const response = await axios.post(`${endpoints.order.create}/${user?.id}`, JSON.stringify({
          listCartId: listCartIdNew,
          discountCode: null,
          isConsignment: isConsignmentUser,
        }), {
          headers: {
            Authorization: `Bearer ${user?.accessToken}`,
            'Content-Type': 'application/json',
          },
        });
        setIsLoading(false);
        setOrderId(response.data?.data?.id); 
        if (response.data.status === 'OK') {
          enqueueSnackbar('Verification needed', { variant: 'info' });
          setOpenDialog(true);
        } 
      } catch (error) {
        enqueueSnackbar('Failed to complete order', { variant: 'error' });
        setIsLoading(false);
        console.error('Failed to complete order', error);
      }
    };
  
    // Form submit handler
    const onSubmiting = () => {
      handleCheckout();
    };


    const handleClickVerify = () => {
      setOpenDialog(true);
    };
  
  

  return (
    <FormProvider methods={methods} >
      <Grid container spacing={3}>
        <Grid xs={12} md={8}>
          <CheckoutDelivery options={DELIVERY_OPTIONS} />

          <CheckoutPaymentMethods
            cardOptions={CARDS_OPTIONS}
            options={PAYMENT_OPTIONS}
            sx={{ my: 3 }}
          />

          <Button
            size="small"
            color="inherit"
            onClick={checkout.onBackStep}
            startIcon={<Iconify icon="eva:arrow-ios-back-fill" />}
          >
            Back
          </Button>
        </Grid>

        <Grid xs={12} md={4}>
          {/* <CheckoutBillingInfo billing={checkout.billing} onBackStep={checkout.onBackStep} /> */}

          <CheckoutSummary
            total={orderTotal}
            subTotal={orderTotal}
            discount={0}
            // shipping={checkout.shipping}
            onEdit={() => checkout.onGotoStep(0)}
          />

          <LoadingButton
            fullWidth
            size="large"
            type="submit"
            variant="contained"
            onClick={handleCheckout}
            loading={isLoading}
          >
            Complete Order
          </LoadingButton>
        </Grid>
        <ModernVerifyView 
        open={openDialog} 
        onClose={handleCloseDialog} 
        user={user} 
        orderId={orderId} 
        onVerifySuccess={onVerifySuccess}
        onNextStep={onNextStep}
        />
      </Grid>
     </FormProvider>
  );
}

