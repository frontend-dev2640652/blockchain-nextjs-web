import PropTypes from 'prop-types';

import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import Stack from '@mui/material/Stack';
import Button from '@mui/material/Button';
import CardHeader from '@mui/material/CardHeader';

import Iconify from 'src/components/iconify';
import { useMockedUser } from 'src/hooks/use-mocked-user';

// ----------------------------------------------------------------------

export default function CheckoutBillingInfo({ billing, onBackStep }) {

  const { user } = useMockedUser();

  console.log(user);

  const fullname = `${user?.userInfoFetch?.userInfo?.firstName} ${user?.userInfoFetch?.userInfo?.lastName}`;

  return (
    <Card sx={{ mb: 3 }}>
      <CardHeader
        title="Address"
        // action={
        //   <Button size="small" startIcon={<Iconify icon="solar:pen-bold" />} onClick={onBackStep}>
        //     Edit
        //   </Button>
        // }
      />
      <Stack spacing={1} sx={{ p: 3 }}>
        <Box sx={{ typography: 'subtitle1' }}>
          {`${fullname} `}
        </Box>
        <Box sx={{ typography: 'subtitle7' }}>
          {`${user?.email} `}
        </Box>
        <Box sx={{ color: 'text.secondary', typography: 'body2' }}>{user?.userInfoFetch?.userInfo?.phoneNumber}</Box>
        <Box sx={{ color: 'text.secondary', typography: 'body2' }}>{user?.userInfoFetch?.userInfo?.address}</Box>
      </Stack>
    </Card>
  );
}

CheckoutBillingInfo.propTypes = {
  billing: PropTypes.object,
  onBackStep: PropTypes.func,
};
