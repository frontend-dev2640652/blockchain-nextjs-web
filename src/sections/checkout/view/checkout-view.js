'use client';

import { useState, useEffect, useCallback, useRouter } from 'react';

import Container from '@mui/material/Container';
import Grid from '@mui/material/Unstable_Grid2';
import Typography from '@mui/material/Typography';

import { useMockedUser } from 'src/hooks/use-mocked-user';

import axios, { endpoints } from 'src/utils/axios';

import { PRODUCT_CHECKOUT_STEPS } from 'src/_mock/_product';

import { useSnackbar } from 'src/components/snackbar';
import { useSettingsContext } from 'src/components/settings';

import CheckoutCart from '../checkout-cart';
import CheckoutSteps from '../checkout-steps';
import { useCheckoutContext } from '../context';
import CheckoutPayment from '../checkout-payment';
import CheckoutOrderComplete from '../checkout-order-complete';
import CheckoutBillingAddress from '../checkout-billing-address';

// ----------------------------------------------------------------------

export default function CheckoutView() {
  const settings = useSettingsContext();

  const checkout = useCheckoutContext();

  const { user } = useMockedUser();

  const { enqueueSnackbar } = useSnackbar();

  const [activeStep, setActiveStep] = useState(0);

  const [orderCompleted, setOrderCompleted] = useState(false);



  const handleNextStep = () => {
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
  };

  const handleCompleteOrder = () => {
    setOrderCompleted(true);
    setActiveStep(PRODUCT_CHECKOUT_STEPS.length - 1);
  };


  useEffect(() => {
    if(orderCompleted) {
      setActiveStep(PRODUCT_CHECKOUT_STEPS.length - 1);
    }
  }, [orderCompleted]);

  return (
    <Container maxWidth={settings.themeStretch ? false : 'lg'} sx={{ mb: 10 }}>
      <Typography variant="h4" sx={{ my: { xs: 3, md: 5 } }}>
        Checkout
      </Typography>

      {/* {activeStep === 0 && (
        <CheckoutCart onNextStep={handleNextStep} />
      )}

      {activeStep === 1 && (
        <CheckoutPayment onNextStep={handleCompleteOrder} />
      )}


      <Grid container justifyContent={checkout.completed ? 'center' : 'flex-start'}>
        <Grid xs={12} md={8}>
        {activeStep === 2 && (
        <CheckoutOrderComplete onNextStep={() => setActiveStep(0)} />
      )}
        </Grid>
      </Grid>

      {checkout.completed ? (
        <CheckoutOrderComplete
          open={checkout.completed}
          onReset={checkout.onReset}
          onDownloadPDF={() => {}}
          
        />
      ) : (
        <>
      {activeStep === 0 && (
        <CheckoutCart onNextStep={handleNextStep} />
      )}
      
      {activeStep === 1 && !orderCompleted && (
      <CheckoutPayment onCompleteOrder={handleCompleteOrder} />
      )}

      

        </>
      )} */}


{
  activeStep === 0 && <CheckoutCart onNextStep={() => setActiveStep(1)} />
}


{
    activeStep === 1 && (
      <CheckoutPayment
        onNextStep={() => setActiveStep(2)}
        onVerifySuccess={handleCompleteOrder} 
      />
    )
  }

  {
    activeStep === PRODUCT_CHECKOUT_STEPS.length - 1 && orderCompleted && (
      <CheckoutOrderComplete
        open={orderCompleted}
        onReset={() => setActiveStep(0)}
        onDownloadPDF={() => {
          // logic cho việc download PDF nếu cần
        }}
      />
    )
  }


    </Container>
  );
}


// {checkout.activeStep === 0 && <CheckoutCart />}

// {/* {checkout.activeStep === 1 && <CheckoutBillingAddress />} */}

// {checkout.activeStep === 1 && checkout.billing && <CheckoutPayment />}