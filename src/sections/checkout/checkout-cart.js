import PropTypes from 'prop-types';
import { useState, useEffect, useCallback } from 'react';

import Card from '@mui/material/Card';
import Button from '@mui/material/Button';
import Grid from '@mui/material/Unstable_Grid2';
import CardHeader from '@mui/material/CardHeader';
import Typography from '@mui/material/Typography';

import { paths } from 'src/routes/paths';
import { RouterLink } from 'src/routes/components';

import { useMockedUser } from 'src/hooks/use-mocked-user';

import axios, { endpoints } from 'src/utils/axios';

import Iconify from 'src/components/iconify';
import { useSnackbar } from 'src/components/snackbar';
import EmptyContent from 'src/components/empty-content';

import { useCheckoutContext } from './context';
import CheckoutSummary from './checkout-summary';
import CheckoutCartProductList from './checkout-cart-product-list';

// ----------------------------------------------------------------------

CheckoutCart.propTypes = {
  onNextStep: PropTypes.func.isRequired,
};

// ----------------------------------------------------------------------

export default function CheckoutCart({ onNextStep }) {
  const checkout = useCheckoutContext();

  const [cartProducts, setCartProducts] = useState([]);

  const [totalProductsCount, setTotalProductsCount] = useState(0);

  const { user } = useMockedUser();

  const { enqueueSnackbar } = useSnackbar();

  const [orderTotal, setOrderTotal] = useState(0);



  const fetchCart = useCallback(async () => {
    const url = `${endpoints.product.list_cart}?userId=${encodeURIComponent(user?.id)}`;
    try {
      const { data } = await axios.get(url, {
        headers: {
          Authorization: `Bearer ${user?.accessToken}`,
          'Content-Type': 'application/json',
        },
      });
  
      if (data?.status === 'OK' || data.data?.status === 'BAD_REQUEST' && Array.isArray(data?.data)) {
        const formattedCartProducts = data?.data?.map((item) => {
          const {product, quantity, price, amount, id} = item;
          return {
            id: product?.id,
            productName: product?.productName,
            price,
            imgUrl: product?.imgUrl,
            unitOfStock: product?.unitOfStock,
            quantity,
            amount,
            cartItemId: id,
          };
        });
        const numberOfDifferentProducts = data?.data?.length;
        setTotalProductsCount(numberOfDifferentProducts);
        setCartProducts(formattedCartProducts);
        const newOrderTotal = data.data.reduce(
          (total, item) => total + item.price * item.quantity,
          0
      );
      setOrderTotal(newOrderTotal);
      enqueueSnackbar(data?.data?.message || 'Product removed from cart successfully', { variant: data?.data?.status === 'BAD_REQUEST' ? 'warning' : 'success' });
      } if (data?.data?.message === "The user's shopping cart does not contain any products") {
        setCartProducts([]);
        setTotalProductsCount(0);
      }  
       else {
        enqueueSnackbar(data.data?.message || 'Unable to fetch cart', { variant: 'error' });
      }
    } catch (error) {
      console.error('Failed to fetch cart', error);
      enqueueSnackbar('Error fetching cart', { variant: 'error' });
    }
  }, [user?.id, user?.accessToken, enqueueSnackbar]);

  useEffect(() => {
    fetchCart();
  }, [fetchCart]);

 
  const handleQuantityChange = useCallback(async (cartItemId, newQuantity) => {
    const url = `${endpoints.product.update_quantity}/${cartItemId}?quantity=${newQuantity}`;
  
    try {
      const response = await axios.put(url, {
        headers: {
          Authorization: `Bearer ${user?.accessToken}`,
          'Content-Type': 'application/json',
        },
      });
      if (response.status === 200) {
        // enqueueSnackbar('Quantity updated successfully', { variant: 'success' });
        fetchCart();
      }
    } catch (error) {
      console.error('Failed to update quantity', error);
      enqueueSnackbar('Error updating quantity', { variant: 'error' });
    }
  }, [user?.accessToken, fetchCart, enqueueSnackbar]);


  const handleDeleteCart = useCallback(async (cartItemId) => {
    try {
      await axios.delete(`${endpoints.product.remove_cart}/${cartItemId}`, {
        headers: {
          Authorization: `Bearer ${user?.accessToken}`,
          'Content-Type': 'application/json',
        }
      });
      fetchCart();
      enqueueSnackbar('Remove product from cart successfully', { variant: 'success' });
    } catch (error) {
      enqueueSnackbar('Some thing went wrong!', { variant: 'error' });

    }
  }, [user?.accessToken, fetchCart, enqueueSnackbar]);


  const handleCheckout = () => {
    onNextStep(); 
  };


  const empty = cartProducts.length === 0;

  return (
    <Grid container spacing={3}>
      <Grid xs={12} md={8}>
        <Card sx={{ mb: 3 }}>
          <CardHeader
            title={
              <Typography variant="h6">
                Cart
                <Typography component="span" sx={{ color: 'text.secondary' }}>
                &nbsp;({totalProductsCount} item{totalProductsCount > 1 ? 's' : ''})
                </Typography>
              </Typography>
            }
            sx={{ mb: 3 }}
          />

          {empty ? (
            <EmptyContent
              title="Cart is Empty!"
              description="Look like you have no items in your shopping cart."
              imgUrl="/assets/icons/empty/ic_cart.svg"
              sx={{ pt: 5, pb: 10 }}
            />
          ) : (
            <CheckoutCartProductList
              products={cartProducts}
              onDelete={(cartItemId) => handleDeleteCart(cartItemId)}
              onIncreaseQuantity={(cartItemId, quantity) => handleQuantityChange(cartItemId, quantity + 1)}
              onDecreaseQuantity={(cartItemId, quantity) => handleQuantityChange(cartItemId, quantity - 1)}
            />
          )}
        </Card>

        <Button
          component={RouterLink}
          href={paths.product.root}
          color="inherit"
          startIcon={<Iconify icon="eva:arrow-ios-back-fill" />}
        >
          Continue Shopping
        </Button>
      </Grid>

      <Grid xs={12} md={4}>
        <CheckoutSummary
          total={orderTotal}
          discount={0}
          subTotal={orderTotal}
          // onApplyDiscount={checkout.onApplyDiscount}
        />

        <Button
          fullWidth
          size="large"
          type="submit"
          variant="contained"
          disabled={empty}
          onClick={handleCheckout}
        >
          Check Out
        </Button>
      </Grid>
    </Grid>
  );
}
