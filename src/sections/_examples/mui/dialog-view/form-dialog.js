import { useState } from 'react';
import PropTypes from 'prop-types';

import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';
import LoadingButton from '@mui/lab/LoadingButton';
import DialogTitle from '@mui/material/DialogTitle';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
// ----------------------------------------------------------------------

FormDialog.propTypes = {
  open: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  publicKey: PropTypes.string.isRequired,
  setPublicKey: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
};

export default function FormDialog({ open, onClose, publicKey, setPublicKey, onSubmit }) {
  const [isSubmitting, setIsSubmitting] = useState(false);

  const handleFormSubmit = async () => {
    setIsSubmitting(true);
    try {
      await onSubmit();
      onClose();
    } catch (error) {
      console.error('Error submitting form:', error);
    }
    setIsSubmitting(false);
  };

  return (
    <Dialog open={open} onClose={onClose}>
      <DialogTitle>Enter your Public Key to verify transaction</DialogTitle>

      <DialogContent>
        <Typography sx={{ mb: 3 }}>
          Warning!: Upon successful key verification, your account will be debited along with the
          amount in the transaction created.
        </Typography>
        <Typography sx={{ mb: 3 }}>
          Do not: Provide your Key to anyone to ensure the safety of your assets!
        </Typography>

        <TextField
          autoFocus
          fullWidth
          type="text"
          margin="dense"
          variant="outlined"
          label="Public Key"
          value={publicKey}
          onChange={(event) => setPublicKey(event.target.value)}
        />
      </DialogContent>

      <DialogActions>
        <Button onClick={onClose} variant="outlined" color="inherit">
          Cancel
        </Button>
        <LoadingButton onClick={handleFormSubmit} variant="contained" loading={isSubmitting}>
          Verify!
        </LoadingButton>
      </DialogActions>
    </Dialog>
  );
}
