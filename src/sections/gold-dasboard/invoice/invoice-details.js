import PropTypes from 'prop-types';

import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import Table from '@mui/material/Table';
import { Container } from '@mui/system';
import Divider from '@mui/material/Divider';
import { styled } from '@mui/material/styles';
import TableRow from '@mui/material/TableRow';
import TableCell from '@mui/material/TableCell';
import TableBody from '@mui/material/TableBody';
import Typography from '@mui/material/Typography';
import TableContainer from '@mui/material/TableContainer';

import { fDate } from 'src/utils/format-time';
import { fCurrency } from 'src/utils/format-number';

import Label from 'src/components/label';
import Scrollbar from 'src/components/scrollbar';
import EmptyContent from 'src/components/empty-content/empty-content';

import InvoiceToolbar from './invoice-toolbar';

// ----------------------------------------------------------------------

// const StyledTableRow = styled(TableRow)(({ theme }) => ({
//   '& td': {
//     textAlign: 'right',
//     borderBottom: 'none',
//     paddingTop: theme.spacing(1),
//     paddingBottom: theme.spacing(1),
//   },
// }));

const StyledTableBody = styled(TableBody)(({ theme }) => ({
  '& td': {
    padding: theme.spacing(1),
    borderBottom: `1px solid ${theme.palette.divider}`,
  },
  '& th': {
    padding: theme.spacing(1),
    textAlign: 'left',
    color: theme.palette.text.secondary,
    borderBottom: `1px solid ${theme.palette.divider}`,
  },
}));

const StyledTableCellImage = styled(TableCell)({
  padding: 0, // Loại bỏ padding nếu muốn kiểm soát kích thước chặt chẽ hơn
  overflow: 'hidden', // Giữ hình ảnh không làm tràn nội dung ra ngoài
  width: '50%', // Đảm bảo mỗi TableCell có chiều rộng không đổi là một nửa
});

// ----------------------------------------------------------------------

export default function InvoiceDetails({ contract }) {
  // const [currentStatus, setCurrentStatus] = useState(contract.status);

  // const handleChangeStatus = useCallback((event) => {
  //   setCurrentStatus(event.target.value);
  // }, []);

  if (!contract) {
    return (
      <Container>
        <EmptyContent title="Please Verify Your Signature" />
      </Container>
    );
  }

  return (
    <Container
      sx={{
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        height: '100%',
        py: 4,
      }}
    >
      <Card sx={{ maxWidth: 1000, width: '100%', mx: 'auto' }}>
        <Scrollbar sx={{ p: 6 }}>
          <Box sx={{ p: 5 }}>
            <Typography variant="h4" sx={{ mb: 3, textAlign: 'center' }}>
              Contract Details
            </Typography>
            <Divider sx={{ my: 2 }} />
            {/* <InvoiceToolbar /> */}
            <TableContainer>
              <Table>
                <StyledTableBody>
                  <TableRow>
                    <TableCell>Contract ID</TableCell>
                    <TableCell>{contract.id}</TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell>Full Name</TableCell>
                    <TableCell>{contract.fullName}</TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell>Address</TableCell>
                    <TableCell>{contract.address}</TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell>Quantity</TableCell>
                    <TableCell>{contract.quantity}</TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell>Price Per Ounce</TableCell>
                    <TableCell>{fCurrency(contract.pricePerOunce)}</TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell>Total Cost/Profit</TableCell>
                    <TableCell>{fCurrency(contract.totalCostOrProfit)}</TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell>Transaction Type</TableCell>
                    <TableCell>{contract.transactionType}</TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell>Created At</TableCell>
                    <TableCell>{fDate(contract.createdAt)}</TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell>Confirming Party</TableCell>
                    <TableCell>{contract.confirmingParty}</TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell>Gold Unit</TableCell>
                    <TableCell>{contract.goldUnit}</TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell>Contract Status</TableCell>
                    <TableCell>
                      <Label
                        color={contract.contractStatus === 'DIGITAL_SIGNED' ? 'success' : 'warning'}
                      >
                        {contract.contractStatus}
                      </Label>
                    </TableCell>
                  </TableRow>

                  <TableRow>
                    <StyledTableCellImage align="center">
                      <Typography variant="subtitle2" gutterBottom>
                        Signature Action Party
                      </Typography>
                      <Box
                        component="img"
                        src={`data:image/jpeg;base64,${contract.signatureActionParty}`}
                        alt="Action Party Signature"
                        sx={{
                          maxHeight: 120,
                          maxWidth: '100%',
                          height: 'auto', // Đảm bảo chiều c.cao tự động điều chỉnh theo chiều rộng
                          objectFit: 'contain',
                          display: 'block',
                          margin: 'auto',
                        }}
                      />
                    </StyledTableCellImage>

                    <StyledTableCellImage align="center">
                      <Typography variant="subtitle2" gutterBottom>
                        Signature Confirming Party
                      </Typography>
                      <Box
                        component="img"
                        src={`data:image/jpeg;base64,${contract.signatureConfirmingParty}`}
                        alt="Confirming Party Signature"
                        sx={{
                          maxHeight: 120,
                          maxWidth: '100%',
                          height: 'auto',
                          objectFit: 'contain',
                          display: 'block',
                          margin: 'auto',
                        }}
                      />
                    </StyledTableCellImage>
                  </TableRow>
                </StyledTableBody>
              </Table>
            </TableContainer>
          </Box>
        </Scrollbar>
      </Card>
    </Container>
  );
}

InvoiceDetails.propTypes = {
  contract: PropTypes.object,
};
