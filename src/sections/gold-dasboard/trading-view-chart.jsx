import React, { memo, useRef, useEffect } from 'react';
import { useSettingsContext } from 'src/components/settings';

function TradingViewChart() {
  const container = useRef();
  const { themeMode } = useSettingsContext();
  const scriptId = 'tradingview-chart-script'; // ID duy nhất cho script

  useEffect(() => {
    if (container.current) {
      // Xác định script cũ nếu có và xóa bỏ
      const existingScript = document.getElementById(scriptId);
      if (existingScript) {
        existingScript.remove();
      }

      const script = document.createElement('script');
      script.src = 'https://s3.tradingview.com/external-embedding/embed-widget-advanced-chart.js';
      script.async = true;
      script.type = 'text/javascript';
      script.id = scriptId; // Thiết lập ID cho script

      // Thành phần chuỗi JSON nên được thiết lập escape chuẩn và sử dụng gạch dưới cho key
      script.innerHTML = JSON.stringify({
        autosize: true,
        symbol: 'FOREXCOM:XAUUSD',
        timezone: 'Asia/Ho_Chi_Minh',
        theme: themeMode === 'dark' ? 'dark' : 'light',
        style: '1',
        locale: 'en',
        enable_publishing: false,
        // "backgroundColor": themeMode === 'dark' ? 'rgba(0, 0, 0, 1)' : 'rgba(255, 255, 255, 1)',
        withdateranges: true,
        range: 'YTD',
        hide_side_toolbar: false,
        allow_symbol_change: true,
        details: true,
        hotlist: true,
        calendar: false,
        watchlist: [
          'FOREXCOM:XAUUSD',
          'FX_IDC:VNDUSD',
          'BITSTAMP:BTCUSD',
          'CRYPTOCAP:ETH',
          'CBOE:VXGOG',
          'NASDAQ:META',
          'NASDAQ:AAPL',
          'ECONOMICS:VNDIR',
          'FX_IDC:VNDJPY',
          'FX_IDC:VNDAUD',
        ],
        support_host: 'https://www.tradingview.com',
      });

      container.current.appendChild(script);
    }

    // Không cần phải đặt cleanup nếu logic xóa bỏ đã chính xác
  }, [themeMode]);

  return (
    <div
      className="tradingview-widget-container"
      ref={container}
      style={{ height: '100%', width: '100%' }}
    >
      <div className="tradingview-widget-container__widget" />
    </div>
  );
}

export default memo(TradingViewChart);
