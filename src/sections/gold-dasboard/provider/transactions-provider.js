'use-client';

import axios, { endpoints } from 'src/utils/axios';

export async function fetchTransactionsList({ userId, accessToken }) {
  try {
    const response = await axios.get(`${endpoints.transactions.list}/${userId}`, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${accessToken}`,
      },
      redirect: 'follow',
    });
    return response.data?.data;
  } catch (error) {
    return error;
  }
}
