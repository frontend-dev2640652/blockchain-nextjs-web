'use-client';

import axios, { endpoints } from 'src/utils/axios';

export async function fetchTransactions({
  userId,
  quantityInOz,
  pricePerOz,
  type,
  goldUnit,
  accessToken,
}) {
  const queryParams = new URLSearchParams({
    quantityInOz,
    pricePerOz,
    type,
    goldUnit,
  }).toString();
  try {
    const response = await axios.put(`${endpoints.transactions.actions}/${userId}?${queryParams}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${accessToken}`,
      },

      redirect: 'follow',
    });
    return response.data;
  } catch (error) {
    return error;
  }
}
