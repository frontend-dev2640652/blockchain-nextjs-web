'use-client';

import axios, { endpoints } from 'src/utils/axios';

export async function fetchConvertTotalAmount({ goldUnit, quantityInOz, pricePerOz }) {
  const queryParams = new URLSearchParams({
    quantityInOz,
    pricePerOz,
    goldUnit,
  }).toString();

  try {
    const response = await axios.get(`${endpoints.transactions.convert}?${queryParams}`, {
      redirect: 'follow',
    });
    return response?.data;
  } catch (error) {
    console.error(`HTTP error! ${error}`);
    throw error;
  }
}
