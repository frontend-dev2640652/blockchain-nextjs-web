import { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

import Chart, { useChart } from 'src/components/chart';

// ----------------------------------------------------------------------

export default function ChartArea({ series, currentRateAnnotation, previousRate }) {
  const currentMaxValue =
    series && series[0] && series[0].data
      ? series[0].data.reduce((max, p) => (p.y > max ? p.y : max), series[0].data[0].y)
      : 0;

  const yaxisMaxValue = Math.max(currentMaxValue, currentRateAnnotation);

  const isIncreased = currentRateAnnotation > previousRate;
  const annotationColor = isIncreased ? '#22C55E' : '#FF5630';

  const chartOptions = useChart({
    yaxis: {
      max: yaxisMaxValue,
    },
    annotations: {
      yaxis: [
        {
          y: currentRateAnnotation,
          borderColor: '#999',
          label: {
            show: true,
            text: `Rates: ${currentRateAnnotation}`,
            style: {
              color: '#fff',
              background: annotationColor,
            },
          },
        },
      ],
    },
    dataLabels: {
      enabled: false,
    },
    markers: {
      size: 0,
      style: 'hollow',
    },
    xaxis: {
      type: 'datetime',
      // min: new Date('01 Mar 2012').getTime(),
      tickAmount: 6,
    },
    tooltip: {
      x: {
        format: 'dd MMM yyyy',
      },
    },
    fill: {
      type: 'gradient',
      gradient: {
        shadeIntensity: 1,
        opacityFrom: 0.7,
        opacityTo: 0.9,
        stops: [0, 100],
      },
    },
    chart: {
      id: 'area-datetime',
      type: 'area',
      height: 350,
      zoom: {
        autoScaleYaxis: true,
        enabled: true,
        type: 'xy',
      },
      toolbar: {
        show: true,
        autoSelected: 'zoom',
        tools: {
          reset: true,
          pan: true,
          zoom: true,
          zoomin: true,
          zoomout: true,
          selection: true,
        },
      },
    },

    pan: {
      enabled: true,
      type: 'xy',
    },
  });

  return (
    <Chart dir="ltr" type="area" series={series} options={chartOptions} width="101%" height={690} />
  );
}

ChartArea.propTypes = {
  series: PropTypes.array,
  currentRateAnnotation: PropTypes.number,
  previousRate: PropTypes.number,
};
