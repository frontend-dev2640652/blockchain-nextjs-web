import PropTypes from 'prop-types';
import { useRef, useMemo, useState, useImperativeHandle } from 'react';

import Box from '@mui/material/Box';
// import Link from '@mui/material/Link';
// import Stack from '@mui/material/Stack';
import { redirect } from 'next/dist/server/api-utils';

// import Avatar from '@mui/material/Avatar';
import Rating from '@mui/material/Rating';
import Popover from '@mui/material/Popover';
import Typography from '@mui/material/Typography';
// import Typography from '@mui/material/Typography';
import {
  DataGrid,
  GridToolbarExport,
  GridActionsCellItem,
  GridToolbarContainer,
  GridToolbarQuickFilter,
  GridToolbarFilterButton,
  GridToolbarColumnsButton,
  GridToolbarDensitySelector,
} from '@mui/x-data-grid';

import { useMockedUser } from 'src/hooks/use-mocked-user';

// import { textAlign } from '@mui/system';
import axios, { endpoints } from 'src/utils/axios';
// import { fPercent } from 'src/utils/format-number';
import { fDate, fTime } from 'src/utils/format-time';

import Label from 'src/components/label';
import Iconify from 'src/components/iconify';
import EmptyContent from 'src/components/empty-content';

// import LinearProgress from '@mui/material/LinearProgress';
import FormDialog from 'src/sections/_examples/mui/dialog-view/form-dialog';

// ----------------------------------------------------------------------

import { Alert, Snackbar } from '@mui/material';

import { useAuth } from 'src/auth/context/jwt/auth-provider';

import FullScreenDialog from './full-screen-dialog';

// const HIDE_COLUMNS = {
//   id: true,
// };

// const HIDE_COLUMNS_TOGGLABLE = ['id', 'actions'];

DataGridCustom.propTypes = {
  fetchData: PropTypes.func.isRequired,
  userId: PropTypes.string.isRequired,
};

export default function DataGridCustom({ data: rows, fetchData, userId }) {
  // const [selectedRows, setSelectedRows] = useState([]);
  const [openDialog, setOpenDialog] = useState(false);

  const [openFullScreenDialog, setOpenFullScreenDialog] = useState(false);

  const [anchorEl, setAnchorEl] = useState(null);

  const [selectedContract, setSelectedContract] = useState(null);

  const { fetchUserInfo } = useAuth();

  // const { user } = useAuth();
  const { user } = useMockedUser();

  const handlePopoverClose = () => {
    setAnchorEl(null);
  };

  const handlePopoverOpen = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const open = Boolean(anchorEl);

  const handleCloseFullScreenDialog = () => {
    setOpenFullScreenDialog(false);
  };

  // const [columnVisibilityModel, setColumnVisibilityModel] = useState(HIDE_COLUMNS);
  const accessToken = user?.accessToken;
  userId = user?.id;

  const [selectedTransactionId, setSelectedTransactionId] = useState(null);
  const [publicKey, setPublicKey] = useState('');

  const handleClickView = (params) => {
    setSelectedTransactionId(params.row.id);
    setOpenDialog(true);
  };

  const handleCloseDialog = () => {
    setOpenDialog(false);
  };
  const encodedPublicKey = encodeURIComponent(publicKey);

  const [openSnackbar, setOpenSnackbar] = useState(false);
  const [snackbarMessage, setSnackbarMessage] = useState('');
  const [snackbarSeverity, setSnackbarSeverity] = useState('info');

  const showSnackbar = (message, severity) => {
    setSnackbarMessage(message);
    setSnackbarSeverity(severity);
    setOpenSnackbar(true);
  };

  const handleCloseSnackbar = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnackbar(false);
  };

  const handleSubmit = async () => {
    const submitdata = await axios.put(
      `${endpoints.transactions.accepted}/${selectedTransactionId}?publicKey=${encodedPublicKey}`,
      {},
      {
        headers: {
          Authorization: `Bearer ${accessToken}`,
          'Content-Type': 'application/json',
        },
      }
    );

    try {
      showSnackbar(submitdata?.data?.message);
    } catch (error) {
      console.error("There's an error while sending request to API", error);
    }

    await fetchData();
    // await fetchUserInfo(user?.accessToken, user?.id);
    setOpenDialog(false);
    setPublicKey('');
  };

  const handleClickViewContract = (params) => {
    // setSelectedTransactionId(params.row.id);
    setSelectedContract(params.row?.contract);
    setOpenFullScreenDialog(true);
  };

  const baseColumns = [
    { field: 'id', headerName: 'ID', width: 70 },
    {
      field: 'transactionType',
      headerName: 'Type',
      headerAlign: 'left',
      width: 100,
      editable: false,
      renderCell: (params) => (
        <Label
          variant="soft"
          color={(params.value === 'BUY' && 'success') || (params.value === 'SELL' && 'error')}
          sx={{ mx: 'auto' }}
        >
          {params.value}
        </Label>
      ),
    },
    {
      field: 'goldUnit',
      headerName: 'Gold Unit',
      headerAlign: 'center',
      align: 'center',
      type: 'singleSelect',
      valueOptions: ['KG', 'MACE', 'TAEL', 'GRAM', 'TROY_OZ'],
      width: 130,
      editable: false,
    },
    {
      field: 'quantity',
      headerName: 'Quantity',
      headerAlign: 'center',
      align: 'center',
      type: 'number',
      width: 90,
      editable: false,
    },
    {
      field: 'pricePerOunce',
      headerName: 'Price Per OZ',
      headerAlign: 'center',
      align: 'center',
      type: 'number',
      width: 150,
      editable: false,
    },
    {
      field: 'totalCostOrProfit',
      headerName: 'Total Cost/Profit',
      headerAlign: 'center',
      align: 'center',
      type: 'number',
      width: 150,
      editable: false,
      renderCell: (params) => {
        const formatNumber = params.value < 0 ? params.value : `+${params.value}`;
        return (
          <Box
            sx={{
              color: params.value < 0 ? 'error.main' : 'success.main',
              fontWeight: 'bold',
            }}
          >
            {formatNumber}
          </Box>
        );
      },
    },
    {
      field: 'createdAt',
      headerName: 'Created At',
      headerAlign: 'center',
      align: 'center',
      type: 'dateTime',
      width: 190,
      valueGetter: (params) => (params.value ? new Date(params.value) : null),
      renderCell: (params) => `${fDate(params.value)} ${fTime(params.value)}`,
    },
    {
      field: 'transactionStatus',
      headerName: 'Transaction Status',
      headerAlign: 'right',
      width: 150,
      renderCell: (params) => (
        <Label
          variant="soft"
          color={
            (params.value === 'PENDING' && 'warning') ||
            (params.value === 'CONFIRMED' && 'success') ||
            (params.value === 'COMPLETED' && 'success')
          }
          sx={{ mx: 'auto' }}
        >
          {params.value}
        </Label>
      ),
    },
    {
      field: 'transactionVerification',
      headerName: 'Verification',
      headerAlign: 'center',
      align: 'center',
      width: 120,
      renderCell: (params) => {
        let icon;
        let color;
        if (params.value === 'UNVERIFIED') {
          icon = 'eva:close-circle-fill';
          color = 'error.main';
        } else if (params.value === 'VERIFIED') {
          icon = 'eva:checkmark-circle-2-fill';
          color = 'primary.main';
        } else {
          icon = 'eva:alert-circle-fill';
          color = 'warning.main';
        }
        return <Iconify icon={icon} sx={{ color }} />;
      },
    },
    {
      field: 'transactionSignature',
      headerName: 'Signature',
      headerAlign: 'center',
      align: 'center',
      width: 120,
      renderCell: (params) => (
        <>
          <Label
            onClick={handlePopoverOpen}
            variant="soft"
            color={
              (params.value === 'UNSIGNED' && 'error') || (params.value === 'SIGNED' && 'success')
            }
            sx={{ mx: 'auto' }}
          >
            {params.value}
          </Label>

          <Popover
            open={open}
            anchorEl={anchorEl}
            onClose={handlePopoverClose}
            anchorOrigin={{
              vertical: 'bottom',
              horizontal: 'center',
            }}
            transformOrigin={{
              vertical: 'top',
              horizontal: 'center',
            }}
            sx={{ '& .MuiPaper-root': { boxShadow: 'none' } }}
          >
            <Box sx={{ p: 2, maxWidth: 280 }}>
              <Typography variant="subtitle1" gutterBottom>
                Verification Signature
              </Typography>
              <Typography variant="body2" sx={{ color: 'text.secondary' }}>
                Please sign your signature in mobile app to create contract
              </Typography>
            </Box>
          </Popover>
        </>
      ),
    },
    {
      field: 'confirmingParty',
      headerName: 'Confirm Party',
      headerAlign: 'center',
      align: 'center',
      width: 130,
      editable: false,
    },
    {
      type: 'actions',
      field: 'actions',
      headerName: 'Actions',
      align: 'center',
      headerAlign: 'center',
      width: 80,
      sortable: false,
      filterable: false,
      disableColumnMenu: true,
    },
  ];

  const customizedColumns = baseColumns.map((col) => {
    if (col.type === 'actions') {
      return {
        ...col,
        getActions: (params) => [
          <GridActionsCellItem
            showInMenu
            icon={<Iconify icon="solar:pen-bold" />}
            label="Verify Transaction"
            onClick={() => handleClickView(params)}
          />,
          <GridActionsCellItem
            showInMenu
            icon={<Iconify icon="solar:eye-bold" />}
            label="View Contract"
            onClick={() => handleClickViewContract(params)}
          />,
          // <GridActionsCellItem
          //   showInMenu
          //   icon={<Iconify icon="solar:trash-bin-trash-bold" />}
          //   label="Delete"
          //   onClick={() => console.info('DELETE', params.row.id)}
          //   sx={{ color: 'error.main' }}
          // />,
        ],
      };
    }
    return col;
  });

  // const getTogglableColumns = () =>
  //   columns
  //     .filter((column) => !HIDE_COLUMNS_TOGGLABLE.includes(column.field))
  //     .map((column) => column.field);

  // const selected = rows.filter((row) => selectedRows.includes(row.id)).map((_row) => _row.id);

  // console.info('SELECTED ROWS', selected);

  return (
    <>
      <DataGrid
        // checkboxSelection
        disableRowSelectionOnClick
        rows={rows}
        columns={customizedColumns}
        // onRowSelectionModelChange={(newSelectionModel) => {
        //   setSelectedRows(newSelectionModel);
        // }}
        // columnVisibilityModel={columnVisibilityModel}
        // onColumnVisibilityModelChange={(newModel) => setColumnVisibilityModel(newModel)}
        slots={{
          toolbar: CustomToolbar,
          noRowsOverlay: () => <EmptyContent title="No Data" />,
          noResultsOverlay: () => <EmptyContent title="No results found" />,
        }}
        slotProps={{
          toolbar: {
            showQuickFilter: true,
          },
          // columnsPanel: {
          //   getTogglableColumns,
          // },
        }}
      />
      <FormDialog
        open={openDialog}
        onClose={handleCloseDialog}
        publicKey={publicKey}
        setPublicKey={setPublicKey}
        onSubmit={handleSubmit}
      />
      {openFullScreenDialog && (
        <FullScreenDialog
          open={openFullScreenDialog}
          onClose={handleCloseFullScreenDialog}
          contract={selectedContract}
        />
      )}
    </>
  );
}

DataGridCustom.propTypes = {
  data: PropTypes.array,
};

// ----------------------------------------------------------------------

function CustomToolbar() {
  return (
    <GridToolbarContainer>
      <GridToolbarQuickFilter />
      <Box sx={{ flexGrow: 1 }} />
      <GridToolbarColumnsButton />
      <GridToolbarFilterButton />
      <GridToolbarDensitySelector />
      <GridToolbarExport />
    </GridToolbarContainer>
  );
}

// ----------------------------------------------------------------------

function RatingInputValue({ item, applyValue, focusElementRef }) {
  const ratingRef = useRef(null);

  useImperativeHandle(focusElementRef, () => ({
    focus: () => {
      ratingRef.current.querySelector(`input[value="${Number(item.value) || ''}"]`).focus();
    },
  }));

  const handleFilterChange = (event, newValue) => {
    applyValue({ ...item, value: newValue });
  };

  return (
    <Rating
      ref={ratingRef}
      precision={0.5}
      value={Number(item.value)}
      onChange={handleFilterChange}
      name="custom-rating-filter-operator"
      sx={{ ml: 2 }}
    />
  );
}

RatingInputValue.propTypes = {
  item: PropTypes.object,
  applyValue: PropTypes.func,
  focusElementRef: PropTypes.any,
};

const ratingOnlyOperators = [
  {
    label: 'Above',
    value: 'above',
    getApplyFilterFn: (filterItem) => {
      if (!filterItem.field || !filterItem.value || !filterItem.operator) {
        return null;
      }

      return (params) => Number(params.value) >= Number(filterItem.value);
    },
    InputComponent: RatingInputValue,
    InputComponentProps: { type: 'number' },
    getValueAsString: (value) => `${value} Stars`,
  },
];
