'use client';

// import { from } from 'stylis';
import PropTypes from 'prop-types';
// eslint-disable-next-line import/no-extraneous-dependencies
import SockJS from 'sockjs-client';
// eslint-disable-next-line import/no-extraneous-dependencies
import { Client } from '@stomp/stompjs';
// import { mt } from 'date-fns/locale';
import { useState, useEffect, useCallback, useRef } from 'react';

import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import Stack from '@mui/material/Stack';
import Alert from '@mui/material/Alert';
import { Snackbar } from '@mui/material';
import Container from '@mui/material/Container';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';
import CardHeader from '@mui/material/CardHeader';
import LoadingButton from '@mui/lab/LoadingButton';
// import Button from '@mui/material/Button';
import { alpha, styled } from '@mui/material/styles';
// import AlertTitle from '@mui/material/AlertTitle';
import Autocomplete from '@mui/material/Autocomplete';
import InputAdornment from '@mui/material/InputAdornment';
import ArrowUpwardIcon from '@mui/icons-material/ArrowUpward';
import ArrowDownwardIcon from '@mui/icons-material/ArrowDownward';

import { useMockedUser } from 'src/hooks/use-mocked-user';
import { useMockedUserInfo } from 'src/hooks/useMockUserInfo';

import axios, { endpoints } from 'src/utils/axios';

import { AuthGuard } from 'src/auth/guard';

import Label from 'src/components/label';
import { useSettingsContext } from 'src/components/settings';

import ChartArea from './chart-area';
import DataGridCustom from './data-grid-custom';
import TradingViewChart from './trading-view-chart';
import { fetchTransactions } from './provider/action-provider';
import { fetchConvertTotalAmount } from './data-filter/price-ounce';
import { fetchTransactionsList } from './provider/transactions-provider';

// ----------------------------------------------------------------------

// const PriceTypography = styled(Typography)(({ theme, isIncreased }) => ({
//   color: isIncreased ? theme.palette.success.main : theme.palette.error.main,
// }));

const PriceTypography = styled(Typography, {
  shouldForwardProp: (prop) => prop !== 'isIncreased',
})(({ theme, isIncreased }) => ({
  color: isIncreased ? theme.palette.success.main : theme.palette.error.main,
}));

function calculateChange(current, previous) {
  if (!current || !previous) return { percentChange: 0, absoluteChange: 0 };
  const change = current - previous;
  const percentChange = (change / previous) * 100;
  return { percentChange: percentChange.toFixed(2), absoluteChange: change.toFixed(2) };
}

export default function GoldDashBoardView({ variant }) {
  const settings = useSettingsContext();

  const { user } = useMockedUser();
  const accessToken = user?.accessToken;

  // const { userInfoFetch, fetchUserInfo } = useMockedUserInfo();

  const [currentPrice, setCurrentPrice] = useState('0');

  const [currentDate, setCurrentDate] = useState('');

  const [seriesData, setSeriesData] = useState([]);

  const [currentRateAnnotation, setCurrentRateAnnotation] = useState(null);

  const [previousRate, setPreviousRate] = useState(null);

  // const ws = useRef(null);

  // const stompClient = useRef(null);

  // sau đó sử dụng stompClient.current để thực thi các hành động

  const [askPrice, setAskPrice] = useState(null);
  const [bidPrice, setBidPrice] = useState(null);

  const [totalAmountBuy, setTotalAmountBuy] = useState('');
  const [totalAmountSell, setTotalAmountSell] = useState('');

  const [isBuying, setIsBuying] = useState(false);
  const [isSelling, setIsSelling] = useState(false);

  const [transactionType, setTransactionType] = useState('');

  const fetchChartData = async () => {
    const response = await axios.get(`${endpoints.metalpriceapi.timeframe}`);
    const apiData = response.data;

    const transformedSeriesData = Object.entries(apiData).map(([date, value]) => ({
      x: new Date(date).getTime(),
      y: value,
    }));

    // Bước 3: Cập nhật state seriesData với dữ liệu đã chuyển đổi
    setSeriesData([{ name: 'Gold Price', data: transformedSeriesData }]);
  };

  // Gọi hàm fetchChartData khi component được mount
  useEffect(() => {
    fetchChartData();
  }, []);

  function requestLatestPrice(client) {
    if (client) {
      client.publish({
        destination: '/app/request-live-rates-forex',
        body: JSON.stringify({ command: 'FETCH_LATEST_PRICE' }),
      });
    }
  }


// https://bgss-company.tech

  useEffect(() => {
    const socket = new SockJS('https://bgss-company.tech/api/auth/ws');
    const stompClient = new Client({
      webSocketFactory: () => socket,
      onConnect: () => {
        console.log('Connected to STOMP server');

        // Assuming this subscription is meant to listen for live rates
        stompClient.subscribe('/api/auth/topic/forexsocket', (message) => {
          const messageData = JSON.parse(message.body);
          const rates = parseFloat(messageData?.mid).toFixed(2);

          console.log(messageData);
          // const updatedPrice = parseFloat(rates.toFixed(2));
          if (currentPrice) {
            setPreviousRate(currentPrice);
          }
          setCurrentPrice(rates);
          // eslint-disable-next-line no-unsafe-optional-chaining
          const date = new Date();
          // messageData?.ts * 1000
          const hours = date.getUTCHours() + 7;
          const minutes = date.getUTCMinutes();
          const seconds = date.getUTCSeconds();
          const paddedHours = String(hours % 24).padStart(2, '0');
          const paddedMinutes = String(minutes).padStart(2, '0');
          const paddedSeconds = String(seconds).padStart(2, '0');
          const formattedTime = `${paddedHours}:${paddedMinutes}:${paddedSeconds}`;

          setCurrentDate(formattedTime);
          setCurrentRateAnnotation(rates);
          setAskPrice(messageData?.ask);
          setBidPrice(messageData?.bid);
          console.log(messageData);
        });
        requestLatestPrice(stompClient);
      },
      onStompError: (frame) => {
        console.error(frame);
      },
      debug: (str) => {
        console.log(str);
      },
    });
    stompClient.activate();
    return () => {
      stompClient.deactivate();
    };
  }, [currentPrice]);

  // useEffect(() => {
  //   const socket = new SockJS('http://93.127.198.196:8080/api/auth/ws');
  //   const stompClient = new Client({
  //     webSocketFactory: () => socket,
  //     onConnect: () => {
  //       console.log('Connected to STOMP server');

  //       stompClient.subscribe('/api/auth/topic/liverates', (message) => {
  //         const messageData = JSON.parse(message.body);
  //         const rates = parseFloat(messageData.rates);
  //         const updatedPrice = parseFloat(rates.toFixed(2));
  //         if (currentPrice) {
  //           setPreviousRate(currentPrice);
  //         }
  //         setCurrentPrice(updatedPrice);
  //         const date = new Date(messageData.timestamp);

  //         const hours = date.getHours().toString().padStart(2, '0');
  //         const minutes = date.getMinutes().toString().padStart(2, '0');
  //         const seconds = date.getSeconds().toString().padStart(2, '0');
  //         const formattedTime = `${hours}:${minutes}:${seconds}`;

  //         setCurrentDate(formattedTime);
  //         setCurrentRateAnnotation(updatedPrice);
  //         console.log(messageData);
  //       });
  //     },
  //     onStompError: (frame) => {
  //       console.error(frame);
  //     },
  //     debug: (str) => {
  //       console.log(str);
  //     },
  //   });
  //   stompClient.activate();
  //   return () => {
  //     stompClient.deactivate();
  //   };
  // }, [currentPrice]);

  const isIncreased = currentPrice > previousRate;

  const { percentChange, absoluteChange } = calculateChange(currentPrice, previousRate);
  // const isIncrease = percentChange > 0;
  const ArrowIcon = isIncreased ? ArrowUpwardIcon : ArrowDownwardIcon;

  const [values, setValues] = useState({
    quantityInOz: '1',
    weight: { title: 'TROY_OZ' },
    weightRange: null,
  });

  const userId = user?.id;

  const { quantityInOz } = values;
  const goldUnit = values?.weightRange?.title;

  // const [totalAmount, setTotalAmount] = useState('');

  useEffect(() => {
    const handleConvert = async () => {
      if (!values.quantityInOz || !values.weightRange?.title) {
        showSnackbar('Please enter both quantity and gold unit', 'warning');
        return;
      }

      // Tính toán cho Buy
      if (askPrice) {
        const conversionBuy = {
          goldUnit: values.weightRange.title,
          quantityInOz: values.quantityInOz,
          pricePerOz: askPrice,
        };
        const fetchedTotalAmountBuy = await fetchConvertTotalAmount(conversionBuy);
        const roundedTotalAmount = fetchedTotalAmountBuy?.data.toFixed(2);
        setTotalAmountBuy(roundedTotalAmount);
      }

      // Tính toán cho Sell
      if (bidPrice) {
        const conversionSell = {
          goldUnit: values.weightRange.title,
          quantityInOz: values.quantityInOz,
          pricePerOz: bidPrice,
        };
        const fetchedTotalAmountSell = await fetchConvertTotalAmount(conversionSell);
        const roundedTotalAmountSell = fetchedTotalAmountSell?.data.toFixed(2);
        setTotalAmountSell(roundedTotalAmountSell);
      }
    };

    handleConvert();
  }, [values.quantityInOz, values.weightRange, askPrice, bidPrice]);

  // if (values.quantityInOz && values.weightRange?.title) {
  //   const conversion = {
  //     goldUnit: values.weightRange.title,
  //     quantityInOz: values.quantityInOz,
  //     pricePerOz: currentPrice ? parseFloat(currentPrice) : 0,
  //   };

  // const fetchedTotalAmount = await fetchConvertTotalAmount(conversion);
  //   try {
  //     setTotalAmount(fetchedTotalAmount?.data);
  //   } catch (error) {
  //     showSnackbar(fetchedTotalAmount?.message, 'error');
  //   }
  // } else {
  //   setTotalAmount('');
  // }

  const handleTransaction = async (type) => {
    if (type === 'BUY') {
      setIsBuying(true);
    } else if (type === 'SELL') {
      setIsSelling(true);
    }

    const pricePerOz = type === 'BUY' ? askPrice : bidPrice;
    if (user?.id === null || (user?.id === undefined && type === 'BUY')) {
      showSnackbar('Please Sign In to BUY', 'warning');
    }

    if (user?.id === null || (user?.id === undefined && type === 'SELL')) {
      showSnackbar('Please Sign In to SELL', 'warning');
    }

    const transactionResponse = await fetchTransactions({
      userId,
      quantityInOz,
      pricePerOz,
      type,
      goldUnit,
      accessToken,
    });
    try {
      const updatedTransactions = await fetchTransactionsList({ userId, accessToken });
      setTransactions(updatedTransactions);
      if (transactionResponse.status === 'OK') {
        showSnackbar(transactionResponse?.message, 'success');
      }
      return fetchTransactions;
    } catch (error) {
      // showSnackbar("", 'error');
      return null;
    } finally {
      if (type === 'BUY') {
        setIsBuying(false);
      } else if (type === 'SELL') {
        setIsSelling(false);
      }
    }
  };

  const [transactions, setTransactions] = useState([]);

  const fetchAndSetTransactions = useCallback(async () => {
    try {
      const data = await fetchTransactionsList({ userId, accessToken });
      setTransactions(data);

      console.log(data);
    } catch (error) {
      console.error(error);
    }
  }, [userId, accessToken]);

  useEffect(() => {
    fetchAndSetTransactions();
  }, [fetchAndSetTransactions]);

  const handleChange = (prop) => (event) => {
    const newValues = { ...values, [prop]: event.target.value };
    setValues(newValues);
  };

  const [openSnackbar, setOpenSnackbar] = useState(false);
  const [snackbarMessage, setSnackbarMessage] = useState('');
  const [snackbarSeverity, setSnackbarSeverity] = useState('info');

  const showSnackbar = (message, severity) => {
    setSnackbarMessage(message);
    setSnackbarSeverity(severity);
    setOpenSnackbar(true);
  };

  const handleCloseSnackbar = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnackbar(false);
  };

  return (
    <Container maxWidth={settings.themeStretch ? false : 'xl'}>
      <Typography variant="h3" textAlign="center" sx={{ my: { xs: 1, md: 5 } }}>
        DashBoard Gold
      </Typography>

      <Box sx={{ mt: 5, display: 'flex', gap: 2 }}>
        <Box
          sx={{
            width: 0.9,
            height: 700,
            borderRadius: 2,
            bgcolor: (theme) => alpha(theme.palette.grey[500], 0.04),
            border: (theme) => `dashed 1px ${theme.palette.divider}`,
          }}
        >
          {/* <ChartArea
            series={seriesData}
            currentRateAnnotation={currentRateAnnotation}
            previousRate={previousRate}
          /> */}
          <TradingViewChart />
        </Box>

        <Box
          sx={{
            width: 0.3,
            height: 500,
            borderRadius: 2,
            bgcolor: (theme) => alpha(theme.palette.primary.main, 0.1),
            border: (theme) => `dashed 1px ${theme.palette.divider}`,
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'space-between',
            p: 3,
          }}
        >
          <Typography variant="h4">Gold</Typography>
          <Typography
            variant="h6"
            sx={{ color: (theme) => alpha(theme.palette.text.primary, 0.5), mb: 2 }}
          >
            XAU/USD
          </Typography>

          <Box
            sx={{
              display: 'flex',
              alignItems: 'center',
              color: isIncreased ? 'success.main' : 'error.main',
            }}
          >
            <PriceTypography variant="h3" isIncreased={isIncreased} sx={{ mb: 2 }}>
              {currentPrice}
            </PriceTypography>
            <ArrowIcon />

            <Typography component="span" sx={{ mx: 2 }}>
              {percentChange}%
            </Typography>

            <Typography component="span">
              ({isIncreased ? '+' : ''}
              {absoluteChange})
            </Typography>
          </Box>
          <Box
            sx={{
              display: 'flex',
              flexDirection: 'column',
              justifyContent: 'space-between',
              alignItems: 'start',
              mb: 2,
            }}
          >
            <Box
              sx={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                gap: 12, // Khoảng cách giữa các thành phần
                mb: 5,
              }}
            >
              <Box
                sx={{
                  // backgroundColor: 'error.main',
                  color: 'common.white',
                  px: 1,
                  py: 0.5,
                  borderRadius: '4px',
                  display: 'flex',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}
              >
                <Label sx={{ backgroundColor: '#CAFDF5', color: '#006C9C' }}>
                  Ask: ${askPrice}
                </Label>
              </Box>

              <Box
                sx={{
                  // backgroundColor: 'success.main',
                  color: 'common.white',
                  px: 1,
                  py: 0.5,
                  borderRadius: '4px',
                  display: 'flex',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}
              >
                <Label sx={{ backgroundColor: '#FFE9D5', color: '#FF5630' }}>
                  Bid: ${bidPrice}
                </Label>
              </Box>
            </Box>

            <Typography variant="h6" color="text.secondary" sx={{ alignSelf: 'center' }}>
              Last updated: {currentDate}
            </Typography>
          </Box>

          <Box sx={{ display: 'flex', alignItems: 'center', mb: 2 }}>
            <TextField
              variant="filled"
              label="Quantity"
              value={values.quantityInOz}
              onChange={(event) => handleChange('quantityInOz')(event)}
              sx={{
                flex: 1,
                mr: 1,
                '& .MuiOutlinedInput-root': {
                  boxShadow: '0 2px 10px 0 rgba(0,0,0,0.25)',
                  backgroundColor: 'background.paper',
                },
              }}
            />
            <Autocomplete
              options={top100Films}
              getOptionLabel={(option) => option.title}
              sx={{ flex: 0.8 }}
              value={values.weightRange || null}
              onChange={(event, newValue) => {
                setValues({ ...values, weightRange: newValue });
              }}
              renderInput={(params) => (
                <TextField
                  {...params}
                  variant="filled"
                  label="Gold Units"
                  sx={{
                    '& .MuiOutlinedInput-root': {
                      boxShadow: '0 2px 10px 0 rgba(0,0,0,0.25)',
                      backgroundColor: 'background.paper',
                    },
                  }}
                />
              )}
            />
          </Box>

          {/* <TextField
            variant="filled"
            fullWidth
            label="Total Amount"
            value={totalAmount}
            sx={{
              '& .MuiOutlinedInput-root': {
                boxShadow: '0 2px 10px 0 rgba(0,0,0,0.25)',
                backgroundColor: 'background.paper', 
              }
            }}         
            InputProps={{
              startAdornment: <InputAdornment position="start">$</InputAdornment>,
            }}
            disabled
            
          /> */}

          <Box
            sx={{
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
              mt: 'auto',
              width: '100%',
            }}
          >
            {/* <AuthGuard> */}
            <LoadingButton
              variant="contained"
              color="success"
              sx={{ width: '48%', mr: '2%' }}
              onClick={() => handleTransaction('BUY')}
              loading={isBuying}
            >
              Buy {totalAmountBuy && `$${totalAmountBuy}`}
            </LoadingButton>
            {/* </AuthGuard> */}
            <LoadingButton
              variant="contained"
              color="error"
              sx={{ width: '48%', ml: '2%' }}
              onClick={() => handleTransaction('SELL')}
              loading={isSelling}
            >
              Sell {totalAmountSell && `$${totalAmountSell}`}
            </LoadingButton>
          </Box>
        </Box>
      </Box>

      <Stack spacing={10} sx={{ mt: 10 }}>
        <Card>
          <CardHeader title="Transactions" sx={{ mb: 2 }} />
          <Box sx={{ height: 720 }}>
            <DataGridCustom data={transactions} fetchData={fetchAndSetTransactions} />
          </Box>
        </Card>
      </Stack>

      <Snackbar
        open={openSnackbar}
        autoHideDuration={6000}
        onClose={handleCloseSnackbar}
        anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
      >
        <Alert onClose={handleCloseSnackbar} severity={snackbarSeverity} sx={{ width: '100%' }}>
          {snackbarMessage}
        </Alert>
      </Snackbar>
    </Container>
  );
}

GoldDashBoardView.propTypes = {
  variant: PropTypes.string,
};

const top100Films = [
  { title: 'KILOGRAM' },
  { title: 'MACE' },
  { title: 'TAEL' },
  { title: 'GRAM' },
  { title: 'TROY_OZ' },
];
