'use client';



import QRCode from 'qrcode.react';
import PropTypes from 'prop-types';

import IconButton from '@mui/material/IconButton';
import { Dialog, Typography, Box } from '@mui/material';
import ContentCopyIcon from '@mui/icons-material/ContentCopy';

import { useSnackbar } from 'src/components/snackbar';

QRcodeView.propTypes = {
  openQr: PropTypes.bool.isRequired,
  onCloseQr: PropTypes.func.isRequired,
  qrcode: PropTypes.string.isRequired,
};

export default function QRcodeView({ openQr, onCloseQr, qrcode }) {

  const { enqueueSnackbar } = useSnackbar();

  const handleCopyToClipboard = () => {
    navigator.clipboard.writeText(qrcode).then(() => {
      enqueueSnackbar('copied to clipboard', { variant: 'success' });
    }).catch(err => {
      enqueueSnackbar('Failed to copy value to clipboard', { variant: 'error' });
    });
  };

  return (
    <Dialog open={openQr} onClose={onCloseQr}>
      <Box sx={{ padding: 1, display: 'flex', alignItems: 'center' }}>  
        <Typography variant="body1" sx={{ marginRight: 0.5, marginLeft: 2.5 }} textAlign='center'>
          {qrcode}
        </Typography>
        <IconButton onClick={handleCopyToClipboard} size="small">
          <ContentCopyIcon fontSize="small" />
        </IconButton>
      </Box>
      <Box sx={{ display: 'flex', justifyContent: 'center', padding: 2 }}>
        <QRCode value={qrcode} size={128} />
      </Box>
    </Dialog>
  );
}