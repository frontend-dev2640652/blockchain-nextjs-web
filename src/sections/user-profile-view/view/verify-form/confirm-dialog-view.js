'use client';

import { useState } from 'react';
import PropTypes from 'prop-types';

import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import LoadingButton from '@mui/lab/LoadingButton';
import CardContent from '@mui/material/CardContent';
import {
  Dialog,
  Button,
  DialogTitle,
  DialogContent,
  DialogActions,
  DialogContentText,
  Box,
  Snackbar,
  Alert,
} from '@mui/material';

import axios, { endpoints } from 'src/utils/axios';

import Editor from './editor/editor';

CancelDialogView.propTypes = {
  openCancel: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired,
  withdrawId: PropTypes.string.isRequired,
  user: PropTypes.object.isRequired,
  onVerifySuccess: PropTypes.func.isRequired,
};

export default function CancelDialogView({
  openCancel,
  onClose,
  withdrawId,
  user,
  onVerifySuccess,
}) {
  const [reasonValue, setReasonValue] = useState('');

  const handleEditorChange = (value) => {
    setReasonValue(value);
  };

  const [openSnackbar, setOpenSnackbar] = useState(false);
  const [snackbarMessage, setSnackbarMessage] = useState('');
  const [snackbarSeverity, setSnackbarSeverity] = useState('info');

  const showSnackbar = (message, severity) => {
    setSnackbarMessage(message);
    setSnackbarSeverity(severity);
    setOpenSnackbar(true);
  };

  const handleCloseSnackbar = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnackbar(false);
  };

  const onSubmitingCancel = async () => {
    try {
      await axios.post(`${endpoints.withdrawal.cancel}/${withdrawId}?reason=${reasonValue}`, {
        headers: {
          Authorization: `Bearer ${user?.accessToken}`,
          'Content-Type': 'application/json',
        },
      });
      onClose();
      onVerifySuccess();
      showSnackbar('Your withdrawal request has been cancellation', 'info');
    } catch (error) {
      showSnackbar('Failed to cancel withdrawal request.', 'error');
    }
  };

  return (
    <>
      <Dialog open={openCancel} onClose={onClose}>
        <DialogTitle>Cancel withdraw</DialogTitle>
        <DialogContent>
          <DialogContentText>Are you sure you want to cancel this withdraw?</DialogContentText>
        </DialogContent>
        <DialogActions>
          <Card sx={{ mb: 3 }}>
            <CardHeader title="Reason Cancel" />
            <CardContent>
              <Editor simple id="simple-editor" onChange={handleEditorChange} />
            </CardContent>
          </Card>
        </DialogActions>
        <DialogActions>
          <Button onClick={onClose} color="primary">
            Cancel
          </Button>
          <LoadingButton
            type="button"
            variant="contained"
            color="primary"
            onClick={onSubmitingCancel}
          >
            Confirm
          </LoadingButton>
        </DialogActions>
      </Dialog>

      <Snackbar
        open={openSnackbar}
        autoHideDuration={5000}
        onClose={handleCloseSnackbar}
        anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
      >
        <Alert onClose={handleCloseSnackbar} severity={snackbarSeverity} sx={{ width: '100%' }}>
          {snackbarMessage}
        </Alert>
      </Snackbar>
    </>
  );
}
