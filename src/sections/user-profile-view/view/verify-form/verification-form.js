'use client';

import * as Yup from 'yup';
import { useState } from 'react';
import PropTypes from 'prop-types';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';

import Link from '@mui/material/Link';
import Stack from '@mui/material/Stack';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import Typography from '@mui/material/Typography';
import LoadingButton from '@mui/lab/LoadingButton';
import DialogTitle from '@mui/material/DialogTitle';
import { Box, Alert, Snackbar } from '@mui/material';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';

import axios, { endpoints } from 'src/utils/axios';

import { EmailInboxIcon } from 'src/assets/icons';

import { useSnackbar } from 'src/components/snackbar';
import FormProvider, { RHFCode } from 'src/components/hook-form';

// ----------------------------------------------------------------------

ModernVerifyView.propTypes = {
  open: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  user: PropTypes.object.isRequired,
  withdrawId: PropTypes.string.isRequired,
  onVerifySuccess: PropTypes.func,
};

export default function ModernVerifyView({ open, onClose, withdrawId, user, onVerifySuccess }) {
  const VerifySchema = Yup.object().shape({
    otp: Yup.string().min(6, 'Code must be at least 6 characters').required('Code is required'),
  });

  const defaultValues = {
    code: '',
  };

  const methods = useForm({
    mode: 'onChange',
    resolver: yupResolver(VerifySchema),
    defaultValues,
  });

  const [snackbarInside, setSnackbarInside] = useState(false);


  const [openSnackbar, setOpenSnackbar] = useState(false);
  const [snackbarMessage, setSnackbarMessage] = useState('');
  const [snackbarSeverity, setSnackbarSeverity] = useState('info');
  const { enqueueSnackbar } = useSnackbar();

  const showSnackbar = (message, severity, insideDialog) => {
    setSnackbarMessage(message);
    setSnackbarSeverity(severity);
    setSnackbarInside(insideDialog);
    setOpenSnackbar(true);
  };

  const handleCloseSnackbar = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnackbar(false);
  };

  const onSubmiting = async (formData) => {
    setSnackbarInside(false);
    try {
      const response = await axios.post(
        `${endpoints.withdrawal.verify}/${formData.otp}/${withdrawId}`,
        {
          headers: {
            Authorization: `Bearer ${user?.accessToken}`,
            'Content-Type': 'application/json',
          },
        }
      );
      onClose();
      // showSnackbar('Your withdrawal request has been verified', 'info');
      onVerifySuccess();
      enqueueSnackbar('Your withdrawal request has been verified', { variant: 'info' });
    } catch (error) {
      // showSnackbar('Invalid or expired OTP! Please try again', 'error');
      enqueueSnackbar('Invalid or expired OTP! Please try again', { variant: 'info' });
    }
  };

  const {
    handleSubmit,
    formState: { isSubmitting },
  } = methods;

  const handleVerifyClick = () => {
    handleSubmit(onSubmiting)();
  };

  const onResent = async () => {
    setSnackbarInside(true);
    try {
      const response = await axios.get(
        `${endpoints.withdrawal.resent}/${withdrawId}`,
        {
          headers: {
            Authorization: `Bearer ${user?.accessToken}`,
            'Content-Type': 'application/json',
          },
        }
      );
      showSnackbar(`One OTP code has been resent to ${user?.email}` , 'info');
      return response?.data;
    } catch (error) {
      if (error.response) {
        console.error(`Server responded with status code ${error.response.status}`);
      } else {
        console.error(error);
      }
      showSnackbar('Something went wrong! Please try again!', 'error');
      return null;
    }
  };


  const renderForm = (
    <Stack spacing={3} alignItems="center">
      <RHFCode name="otp" label="Code" />
      <Typography variant="body2">
        {`Don’t have a code? `}
        <Link
          variant="subtitle2"
          sx={{
            cursor: 'pointer',
          }}
          onClick={() => onResent()}
        >
          Resend code
        </Link>
      </Typography>
    </Stack>
  );

  const renderHead = (
    <>
      <EmailInboxIcon sx={{ height: 96 }} />

      <Stack spacing={1} sx={{ mt: 3, mb: 5 }}>
        <Typography variant="h3">Please check your email!</Typography>

        <Typography variant="body2" sx={{ color: 'text.secondary' }}>
          We have emailed a 6-digit confirmation code to {user?.email}, please enter the code in
          below box to verify your withdraw.
        </Typography>
      </Stack>
    </>
  );

  return (
    <>
    <Dialog open={open} onClose={onClose}>
      <FormProvider {...methods}>
        <DialogTitle>Withrawal Verification</DialogTitle>

        <DialogContent>
          <Box sx={{ width: '100%', maxWidth: 500, mx: 'auto' }}>
            <Stack spacing={3} alignItems="center">
              <FormProvider methods={methods}>
                {renderHead}

                {renderForm}
              </FormProvider>
            </Stack>
          </Box>
        </DialogContent>

        <DialogActions>
          <Button onClick={onClose} variant="outlined" color="inherit">
            Cancel
          </Button>
          <LoadingButton
            type="button"
            variant="contained"
            loading={isSubmitting}
            onClick={handleVerifyClick}
          >
            Verify!
          </LoadingButton>
        </DialogActions>
      </FormProvider>

      {/* {snackbarInside === && ( */}
          <Snackbar
            open={openSnackbar}
            autoHideDuration={5000}
            onClose={handleCloseSnackbar}
          >
            <Alert onClose={handleCloseSnackbar} severity={snackbarSeverity} sx={{ width: '100%' }}>
              {snackbarMessage}
            </Alert>
          </Snackbar>
        {/* )} */}
    </Dialog>
    {/* {snackbarInside === && (
        <Snackbar
          open={openSnackbar}
          autoHideDuration={5000}
          onClose={handleCloseSnackbar}
          anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
        >
          <Alert onClose={handleCloseSnackbar} severity={snackbarSeverity} sx={{ width: '100%' }}>
            {snackbarMessage}
          </Alert>
        </Snackbar>
      )} */}
    </>
  );
}
