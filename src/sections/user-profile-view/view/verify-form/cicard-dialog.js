'use client';

import PropTypes from 'prop-types';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { useState, useEffect, useCallback } from 'react';

import Link from '@mui/material/Link';
import Stack from '@mui/material/Stack';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import Typography from '@mui/material/Typography';
import LoadingButton from '@mui/lab/LoadingButton';
import DialogTitle from '@mui/material/DialogTitle';
import { Box, Snackbar, Alert } from '@mui/material';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';

import { fData } from 'src/utils/format-number';
import axios, { endpoints } from 'src/utils/axios';

import { Upload } from 'src/components/upload';
import FormProvider, { RHFSwitch, RHFTextField, RHFUploadAvatar } from 'src/components/hook-form';
import { useSnackbar } from 'src/components/snackbar';

CICardDialogView.propTypes = {
  open: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  user: PropTypes.object.isRequired,
  onVerifySuccess: PropTypes.func,
};

export default function CICardDialogView({ open, onClose, user, onVerifySuccess }) {
  const [frontImage, setFrontImage] = useState(null);

  const [backImage, setBackImage] = useState(null);

  const { enqueueSnackbar } = useSnackbar();

  const [isVerifying, setIsVerifying] = useState(false);

  const handleDropSingleFrontImage = useCallback((acceptedFiles) => {
    const newFile = acceptedFiles[0];
    if (newFile) {
      setFrontImage(
        Object.assign(newFile, {
          preview: URL.createObjectURL(newFile),
        })
      );
    }
  }, []);

  const handleDropSingleBackImage = useCallback((acceptedFiles) => {
    const newFile = acceptedFiles[0];
    if (newFile) {
      setBackImage(
        Object.assign(newFile, {
          preview: URL.createObjectURL(newFile),
        })
      );
    }
  }, []);

  const methods = useForm({
    resolver: yupResolver(),
  });

  const {
    setValue,
    handleSubmit,
    watch,
    formState: { isSubmitting },
  } = methods;

  const handleVerifyClick = async () => {
    if (!frontImage || !backImage) {
      enqueueSnackbar('Please provide both front and back images.', { variant: 'warning' });
      return;
    }

    setIsVerifying(true);

    const formData = new FormData();
    formData.append('frontImage', frontImage);
    formData.append('backImage', backImage);

    try {
      const response = await axios.post(`${endpoints.users.verifycicard}/${user?.id}`, formData, {
        headers: {
          'Content-Type': 'multipart/form-data',
          Authorization: `Bearer ${user?.accessToken}`,
        },
      });

      // Gọi hàm onVerifySuccess với thông tin người dùng được trả về từ API
      // để cập nhật các fields trong AccountGeneral
      if (response.data) {
        onClose();
        onVerifySuccess(response?.data);
        enqueueSnackbar('Your CI Card has been verified! Check Your Infomation!', { variant: 'success' });
      }
    } catch (error) {
      console.error(error);
      enqueueSnackbar('Failed to verify CI Card.', { variant: 'error' });
    }
    setIsVerifying(false);
  };

  return (
    <>
      <Dialog open={open} onClose={onClose} fullWidth maxWidth="md">
        <FormProvider {...methods}>
          <DialogTitle>Citizen Identification Card Verification</DialogTitle>

          <DialogContent>
            <Box sx={{ mx: 'auto', textAlign: 'center' }}>
              <Stack direction="row" spacing={5} alignItems="center" justifyContent="center">
                <Box>
                  <Typography variant="subtitle1" gutterBottom>
                    Front Image
                  </Typography>
                  <Upload
                    file={frontImage}
                    onDrop={handleDropSingleFrontImage}
                    onDelete={() => setFrontImage(null)}
                    helperText={
                      <Typography
                        variant="caption"
                        sx={{
                          mt: 3,
                          mx: 'auto',
                          display: 'block',
                          textAlign: 'center',
                          color: 'text.disabled',
                        }}
                      >
                        Allowed *.jpeg, *.jpg, *.png, *.gif
                        <br /> max size of {fData(3145728)}
                      </Typography>
                    }
                  />
                </Box>

                <Box>
                  <Typography variant="subtitle1" gutterBottom>
                    Back Image
                  </Typography>
                  <Upload
                    file={backImage}
                    onDrop={handleDropSingleBackImage}
                    onDelete={() => setBackImage(null)}
                    helperText={
                      <Typography
                        variant="caption"
                        sx={{
                          mt: 3,
                          mx: 'auto',
                          display: 'block',
                          textAlign: 'center',
                          color: 'text.disabled',
                        }}
                      >
                        Allowed *.jpeg, *.jpg, *.png, *.gif
                        <br /> max size of {fData(3145728)}
                      </Typography>
                    }
                  />
                </Box>
              </Stack>
            </Box>
          </DialogContent>

          <DialogActions>
            <Button onClick={onClose} variant="outlined" color="inherit">
              Cancel
            </Button>
            <LoadingButton
              type="button"
              variant="contained"
              loading={isVerifying}
              onClick={handleVerifyClick}
            >
              Verify!
            </LoadingButton>
          </DialogActions>
        </FormProvider>
      </Dialog>

      {/* <Snackbar
        open={openSnackbar}
        autoHideDuration={5000}
        onClose={handleCloseSnackbar}
        anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
      >
        <Alert onClose={handleCloseSnackbar} severity={snackbarSeverity} sx={{ width: '100%' }}>
          {snackbarMessage}
        </Alert>
      </Snackbar> */}
    </>
  );
}
