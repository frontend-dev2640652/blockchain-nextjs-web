'use client'


import { useEffect, useState, useCallback } from 'react';

import Card from '@mui/material/Card';
import CancelIcon from '@mui/icons-material/Cancel';
import { Box, Stack, Popover, Typography } from '@mui/material';
import QrCodeScannerIcon from '@mui/icons-material/QrCodeScanner';
import {
  DataGrid,
  GridToolbarExport,
  GridActionsCellItem,
  GridToolbarContainer,
  GridToolbarQuickFilter,
  GridToolbarFilterButton,
  GridToolbarColumnsButton,
  GridToolbarDensitySelector,
} from '@mui/x-data-grid';

import { useMockedUser } from 'src/hooks/use-mocked-user';

import axios, { endpoints } from 'src/utils/axios';
import { fDate, fTime } from 'src/utils/format-time';

import Label from 'src/components/label/label';
import Iconify from 'src/components/iconify/iconify';
import EmptyContent from 'src/components/empty-content';
import FormProvider from 'src/components/hook-form/form-provider';

import ModernVerifyView from 'src/sections/checkout/verification-form';

import QRcodeView from './verify-form/qrcode-view';
import InvoiceDetails from './invoice-order/invoice-details';
import CancelDialogView from './verify-form/confirm-dialog-view';
import FullScreenDialog from './invoice-order/full-screen-dialog';






export default function OrderDetailsView() {

    const { user } = useMockedUser();

    const [orderList, setOrderList] = useState([]);

    const [anchorEl, setAnchorEl] = useState(null);

    const [openDialog, setOpenDialog] = useState(false);

    const [orderId, setOrderId] = useState(null);

    const [openFullScreenDialog, setOpenFullScreenDialog] = useState(false);

    const handleCloseFullScreenDialog = () => {
        setOpenFullScreenDialog(false);
      };

    const open = Boolean(anchorEl);

    const handlePopoverOpen = (event) => {
        setAnchorEl(event.currentTarget);
      };

      const handlePopoverClose = () => {
        setAnchorEl(null);
      };

      const handleClickVerify = (params) => {
        setOrderId(params.row.id);
        setOpenDialog(true);
      };
    

      const handleClickOrder = (params) => {
        setOrderId(params.row.id);
        setOpenFullScreenDialog(true);
      };


    const fetchOrderList = useCallback(async () => {
        try {
          const response = await axios.get(`${endpoints.order.list}?userId=${user?.id}`, {
            headers: {
              Authorization: `Bearer ${user?.accessToken}`,
              'Content-Type': 'application/json',
            },
          });
          setOrderList(response?.data?.data);
        } catch (error) {
          console.error('Error fetching withdrawals:', error);
        }
      }, [user?.id, user?.accessToken]); // Dependencies array
    
      // Gọi hàm fetchWithdraws khi component mounts và dependencies thay đổi.
      useEffect(() => {
        fetchOrderList();
      }, [fetchOrderList]);

      const handleCloseDialog = () => {
        setOpenDialog(false);
      };

      const onVerificationSuccess = useCallback(() => {
        fetchOrderList(); // Gọi lại hàm fetchOrderList để cập nhật danh sách mới
      }, [fetchOrderList]); 



    




    const baseColumns = [
        { field: 'id', headerName: 'ID', width: 70 },
        {
          field: 'totalAmount',
          headerName: 'Total Amount',
          headerAlign: 'center',
          align: 'center',
          type: 'number',
          width: 230,
          editable: false,
            renderCell: (params) => `${params.value.toLocaleString()}$`,
        },
        {
            field: 'discountCode',
            headerName: 'Discount Code',
            headerAlign: 'center',
            align: 'center',
            width: 200,
            renderCell: (params) => params.value || 'None',
          },
        {
          field: 'createDate',
          headerName: 'Created At',
          headerAlign: 'center',
          align: 'center',
          type: 'dateTime',
          width: 250,
          valueGetter: (params) => (params.value ? new Date(params.value) : null),
          renderCell: (params) => `${fDate(params.value)} ${fTime(params.value)}`,
        },
        {
          field: 'statusReceived',
          headerName: 'Status',
          headerAlign: 'center',
          align: 'center',
          width: 250,
          renderCell: (params) => (
            <>
              <Label
                onClick={handlePopoverOpen}
                variant="soft"
                color={
                  (params.value === 'UNVERIFIED' && 'error') ||
                  (params.value === 'RECEIVED' && 'success') ||
                  (params.value === 'NOT_RECEIVED' && 'warning')
                }
                sx={{ mx: 'auto' }}
              >
                {params.value}
              </Label>
              <Popover
                open={open}
                anchorEl={anchorEl}
                onClose={handlePopoverClose}
                anchorOrigin={{
                  vertical: 'bottom',
                  horizontal: 'center',
                }}
                transformOrigin={{
                  vertical: 'top',
                  horizontal: 'center',
                }}
                sx={{ '& .MuiPaper-root': { boxShadow: 'none' } }}
              >
                <Box sx={{ p: 2, maxWidth: 280 }}>
                  <Typography variant="subtitle1" gutterBottom>
                    Verification Order
                  </Typography>
                  <Typography variant="body2" sx={{ color: 'text.secondary' }}>
                    Please attach the action to sent the OTP to verification!
                  </Typography>
                </Box>
              </Popover>
            </>
          ),
        },
    
        {
          type: 'actions',
          field: 'actions',
          headerName: 'Actions',
          align: 'center',
          headerAlign: 'center',
          width: 100,
          sortable: false,
          filterable: false,
          disableColumnMenu: true,
        },
      ];
    
      const customizedColumns = baseColumns.map((col) => {
        if (col.type === 'actions') {
          return {
            ...col,
            getActions: (params) => [
              <GridActionsCellItem
                showInMenu
                icon={<Iconify icon="solar:pen-bold" />}
                label="Verify Order"
                onClick={() => handleClickVerify(params)}
              />,
              <GridActionsCellItem
                showInMenu
                icon={<Iconify icon="solar:eye-bold" />}
                label="View Order Details"
                onClick={() => handleClickOrder(params)}
              />,
            ],
          };
        }
        return col;
      });
    
      return (
        <FormProvider>
          <Stack component={Card} spacing={3} sx={{ p: 3 }}>
            <DataGrid
              disableRowSelectionOnClick
              rows={orderList}
              columns={customizedColumns}
              slots={{
                toolbar: CustomToolbar,
                noRowsOverlay: () => <EmptyContent title="No Data" />,
                noResultsOverlay: () => <EmptyContent title="No results found" />,
              }}
              slotProps={{
                toolbar: {
                  showQuickFilter: true,
                },
                // columnsPanel: {
                //   getTogglableColumns,
                // },
              }}
            />
            <ModernVerifyView
              open={openDialog}
              onClose={handleCloseDialog}
              orderId={orderId}
              user={user}
              onVerifySuccess={onVerificationSuccess}
            />
            <FullScreenDialog 
            open={openFullScreenDialog} 
            onClose={handleCloseFullScreenDialog} 
            user={user} 
            orderId={orderId}/>

          </Stack>
        </FormProvider>
      );
    }
    
    function CustomToolbar() {
      return (
        <GridToolbarContainer>
          <GridToolbarQuickFilter />
          <Box sx={{ flexGrow: 1 }} />
          <GridToolbarColumnsButton />
          <GridToolbarFilterButton />
          <GridToolbarDensitySelector />
          <GridToolbarExport />
        </GridToolbarContainer>
      );
}
