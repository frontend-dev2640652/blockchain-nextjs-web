'use client';

import * as Yup from 'yup';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { useState, useEffect, useCallback } from 'react';

import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import Stack from '@mui/material/Stack';
import Button from '@mui/material/Button';
import Grid from '@mui/material/Unstable_Grid2';
import Typography from '@mui/material/Typography';
import LoadingButton from '@mui/lab/LoadingButton';
import { Tooltip, IconButton, InputAdornment } from '@mui/material';

import { useMockedUser } from 'src/hooks/use-mocked-user';
import { useCopyToClipboard } from 'src/hooks/use-copy-to-clipboard';

import { fData } from 'src/utils/format-number';
import axios, { endpoints } from 'src/utils/axios';

import Iconify from 'src/components/iconify/iconify';
import { useSnackbar } from 'src/components/snackbar';
import FormProvider, { RHFSwitch, RHFTextField, RHFUploadAvatar } from 'src/components/hook-form';

import CICardDialogView from './verify-form/cicard-dialog';
import { getSecretKey } from '../account-provider/secret-key-provider';

// import { useState } from 'react';

// ----------------------------------------------------------------------

export default function AccountGeneral() {
  const { enqueueSnackbar } = useSnackbar();
  const { user } = useMockedUser();

  const { copy } = useCopyToClipboard();
  const [publicKey, setPublicKey] = useState('');

  const isVerified = publicKey !== '';

  const [openDialogCicard, setOpenDialogCICard] = useState(false);

  const handleCloseDialogCiCard = () => {
    setOpenDialogCICard(false);
  };

  const handleOpenDialogCiCard = () => {
    setOpenDialogCICard(true);
  };

  useEffect(() => {
    if (user?.id) {
      getSecretKey(user.id)
        .then((data) => {
          setPublicKey(data.publicKey);
        })
        .catch((error) => {
          enqueueSnackbar('Please verify you CI card to get Key!', { variant: 'warning' });
        });
    }
  }, [user?.id, enqueueSnackbar]);

  const UpdateUserSchema = Yup.object().shape({
    fullName: Yup.string().required('Name is required'),
    email: Yup.string().required('Email is required').email('Email must be a valid email address'),
    photoURL: Yup.mixed().nullable(),
    phoneNumber: Yup.string().required('Phone number is required'),
    address: Yup.string().required('Address is required'),
    dob: Yup.string().required('Dob is required'),
    ciCardNumber: Yup.string().required('CiCard Number is required'),
    // not required
    // isPublic: Yup.boolean(),
  });

  const firstNameMod = user?.userInfoFetch.userInfo.firstName || '';
  const lastNameMod = user?.userInfoFetch.userInfo.lastName || '';
  const fullNameMod = `${firstNameMod} ${lastNameMod}`.trim();

  const splitName = (fullName) => {
    const parts = fullName.trim().split(/\s+/);
    const lastName = parts.pop();
    const firstName = parts.join(' ');
    return { firstName, lastName };
  };

  const defaultValues = {
    fullName: fullNameMod || '',
    email: user?.email || '',
    photoURL: `/assets/images/${user?.userInfoFetch?.userInfo?.avatarData?.imgUrl}` || null,
    phoneNumber: user?.userInfoFetch.userInfo.phoneNumber || '',
    address: user?.userInfoFetch.userInfo.address || '',
    dob: user?.userInfoFetch.userInfo.doB || '',
    ciCardNumber: user?.userInfoFetch.userInfo.ciCard || '',
  };

  const methods = useForm({
    resolver: yupResolver(UpdateUserSchema),
    defaultValues,
  });

  const {
    setValue,
    handleSubmit,
    watch,
    formState: { isSubmitting },
  } = methods;

  const onSubmit = handleSubmit(async (data) => {
    try {
      await new Promise((resolve) => setTimeout(resolve, 500));
      enqueueSnackbar('Update success!');
      console.info('DATA', data);
    } catch (error) {
      console.error(error);
    }
  });

  function formatDate(date) {
    if (!date) return;

    const d = date instanceof Date ? date : new Date(date);
    const day = `0${d.getDate()}`.slice(-2);
    const month = `0${d.getMonth() + 1}`.slice(-2);
    const year = d.getFullYear();

    // eslint-disable-next-line consistent-return
    return [year, month, day].join('-');
  }

  const onSubmitUserInfo = handleSubmit(async (formData) => {
    const { firstName, lastName } = splitName(formData.fullName);
    const formattedDateOfBirth = formatDate(formData.dob);
    const userInfoPayload = {
      firstName,
      lastName,
      phoneNumber: formData.phoneNumber,
      doB: formattedDateOfBirth,
      address: formData.address,
      ciCard: formData.ciCardNumber,
    };

    try {
      const response = await axios({
        method: 'put',
        url: `${endpoints.users.update}/${user?.id}`,
        data: userInfoPayload,
        headers: {
          Authorization: `Bearer ${user.accessToken}`,
          'Content-Type': 'application/json',
        },
      });

      if (response.data) {
        enqueueSnackbar('User info updated successfully! Please Re-sign to updated profile', {
          variant: 'success',
        });
      }
    } catch (error) {
      enqueueSnackbar('Failed to update user info.', { variant: 'error' });
      console.error(error);
    }
  });

  const handleUploadImage = async (selectedFiles) => {
    const file = watch('photoURL');
    if (!file) {
      enqueueSnackbar('Please select an image to upload', { variant: 'warning' });
      return;
    }

    const formData = new FormData();
    formData.append('imageData', file);

    try {
      await axios.put(`${endpoints.users.avatar}/${user?.id}`, formData, {
        headers: {
          Authorization: `Bearer ${user?.accessToken}`,
        },
      });
      enqueueSnackbar('Image uploaded successfully', { variant: 'success' });
    } catch (error) {
      enqueueSnackbar('Failed to upload image', { variant: 'error' });
      console.error(error);
    }
  };

  // Trong AccountGeneral.js

  const onVerifySuccess = (dataAPI) => {
    console.log('Updating full name:', `${dataAPI.firstName} ${dataAPI.lastName}`);
    setValue('fullName', `${dataAPI.firstName} ${dataAPI.lastName}`);
    setValue('dob', new Date(dataAPI.dob).toISOString().substring(0, 10));
    setValue('ciCardNumber', dataAPI.ciCard);
    setValue('address', dataAPI.address);
    enqueueSnackbar('Verification successful!', { variant: 'success' });
    console.log(dataAPI.firstName);
  };

  const handleDrop = useCallback(
    (acceptedFiles) => {
      const file = acceptedFiles[0];

      const newFile = Object.assign(file, {
        preview: URL.createObjectURL(file),
      });

      if (file) {
        setValue('photoURL', newFile, { shouldValidate: true });
      }
    },
    [setValue]
  );

  const [value, setValuePublicKey] = useState(defaultValues.publicKey);

  const onCopy = useCallback(
    (text) => {
      if (text) {
        enqueueSnackbar('Copied!');
        copy(text);
      }
    },
    [copy, enqueueSnackbar]
  );

  const handleChange = useCallback(
    (event) => {
      setValuePublicKey(event.target.setValue);
    },
    [setValuePublicKey]
  );

  function obfuscateKey(key) {
    if (!key || key === null) return publicKey;
    const visiblePart = key.substring(0, 20);
    return `${visiblePart}*********************************************************`;
  }

  return (
    <FormProvider methods={methods} onSubmit={onSubmit}>
      <Grid container spacing={3}>
        <Grid xs={12} md={4}>
          <Card sx={{ pt: 10, pb: 5, px: 3, textAlign: 'center' }}>
            <RHFUploadAvatar
              name="photoURL"
              // src={photoURL}
              maxSize={3145728}
              onDrop={handleDrop}
              helperText={
                <Typography
                  variant="caption"
                  sx={{
                    mt: 3,
                    mx: 'auto',
                    display: 'block',
                    textAlign: 'center',
                    color: 'text.disabled',
                  }}
                >
                  Allowed *.jpeg, *.jpg, *.png, *.gif
                  <br /> max size of {fData(3145728)}
                </Typography>
              }
            />

            <LoadingButton
              variant="contained"
              component="label"
              sx={{ mt: 2 }}
              onClick={handleUploadImage}
              disabled={isSubmitting}
            >
              Upload Image
            </LoadingButton>

            <RHFSwitch
              name="isPublic"
              labelPlacement="start"
              label="Public Profile"
              sx={{ mt: 5 }}
            />
            <Button
              variant="contained"
              sx={{
                mt: 3,
                bgcolor: isVerified ? 'success.main' : 'warning.main',
                color: 'common.white', // Set màu chữ thành trắng hoặc màu khác tùy ý
                '&:hover': {
                  bgcolor: isVerified ? 'success.dark' : 'warning.dark',
                },
                // Thêm style tùy chỉnh cho nút disabled
                '&.Mui-disabled': {
                  bgcolor: isVerified ? 'success.main' : 'warning.light', // Background màu xám nhẹ hơn nếu chưa verified
                  color: isVerified ? 'common.white' : 'text.secondary', // Màu chữ tự chọn nếu chưa verified
                },
              }}
              onClick={handleOpenDialogCiCard}
              disabled={isVerified}
            >
              {isVerified ? 'CI Card Verified' : 'Verify CI Card'}
            </Button>
            <CICardDialogView
              open={openDialogCicard}
              onClose={handleCloseDialogCiCard}
              user={user}
              onVerifySuccess={onVerifySuccess}
            />
          </Card>
        </Grid>

        <Grid xs={12} md={8}>
          <Card sx={{ p: 3 }}>
            <Box
              rowGap={3}
              columnGap={2}
              display="grid"
              gridTemplateColumns={{
                xs: 'repeat(1, 1fr)',
                sm: 'repeat(2, 1fr)',
              }}
            >
              <RHFTextField name="fullName" label="Full Name" />
              <RHFTextField name="email" label="Email Address" />
              <RHFTextField name="phoneNumber" label="Phone Number" />
              <RHFTextField name="address" label="Address" />

              {/* <RHFAutocomplete
                name="country"
                type="country"
                label="Country"
                placeholder="Choose a country"
                options={countries.map((option) => option.label)}
                getOptionLabel={(option) => option}
              /> */}
              {/* 
              <RHFTextField name="state" label="State/Region" /> */}
              {/* <RHFTextField name="city" label="City" /> */}
              <RHFTextField name="dob" label="Date of Birth" />
              {/* <Controller
                name="dob"
                control={control}
                render={({ field, fieldState: { error } }) => (
                  <DatePicker
                    {...field}
                    label="Date of Birth"
                    format="yyyy-MM-dd"
                    slotProps={{
                      textField: {
                        fullWidth: true,
                        error: !!error,
                        helperText: error?.message,
                      },
                    }}
                  />
                )}
              /> */}
              {/* <DesktopDatePicker
          label="For desktop"
          value={value}
          minDate={new Date('2017-01-01')}
          onChange={(newValue) => {
            setValue(newValue);
          }}
          slotProps={{
            textField: {
              fullWidth: true,
              margin: 'normal',
            },
          }}
        /> */}
              <RHFTextField name="ciCardNumber" label="CiCard Number" />
            </Box>

            <Stack spacing={3} alignItems="flex-end" sx={{ mt: 3 }}>
              <RHFTextField
                name="publicKey"
                multiline
                rows={1}
                label="Public Key"
                value={obfuscateKey(publicKey)}
                onchange={handleChange}
                InputLabelProps={{
                  shrink: true,
                }}
                disabled
                // InputProps={{
                //   readOnly: true,
                // }}
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="end">
                      <Tooltip title="Copy">
                        <IconButton onClick={() => onCopy(publicKey)}>
                          <Iconify icon="eva:copy-fill" width={24} />
                        </IconButton>
                      </Tooltip>
                    </InputAdornment>
                  ),
                }}
              />

              <LoadingButton
                type="submit"
                variant="contained"
                loading={isSubmitting}
                onClick={onSubmitUserInfo}
              >
                Save Changes
              </LoadingButton>
            </Stack>
          </Card>
        </Grid>
      </Grid>
    </FormProvider>
  );
}
