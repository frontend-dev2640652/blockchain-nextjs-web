import PropTypes from 'prop-types';
import { useState, useEffect, useCallback } from 'react';

import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import Table from '@mui/material/Table';
import Stack from '@mui/material/Stack';
import { IconButton } from '@mui/material';
import Divider from '@mui/material/Divider';
import { styled } from '@mui/material/styles';
import TableRow from '@mui/material/TableRow';
import TableHead from '@mui/material/TableHead';
import TableCell from '@mui/material/TableCell';
import TableBody from '@mui/material/TableBody';
import Grid from '@mui/material/Unstable_Grid2';
import Typography from '@mui/material/Typography';
import TableContainer from '@mui/material/TableContainer';
import QrCodeScannerIcon from '@mui/icons-material/QrCodeScanner';

import { useMockedUser } from 'src/hooks/use-mocked-user';

import { fDate } from 'src/utils/format-time';
import axios, { endpoints } from 'src/utils/axios';
import { fCurrency } from 'src/utils/format-number';

import { INVOICE_STATUS_OPTIONS } from 'src/_mock';

import Label from 'src/components/label';
import Scrollbar from 'src/components/scrollbar';

import InvoiceToolbar from './invoice-toolbar';
import QRcodeView from '../verify-form/qrcode-view';

// ----------------------------------------------------------------------

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  '& td': {
    textAlign: 'right',
    borderBottom: 'none',
    paddingTop: theme.spacing(1),
    paddingBottom: theme.spacing(1),
  },
}));

// ----------------------------------------------------------------------

export default function InvoiceDetails({ orderId }) {

  const { user } = useMockedUser();

  // const handleChangeStatus = useCallback((event) => {
  //   setCurrentStatus(event.target.value);
  // }, []);

  const [invoice, SetInvoice] = useState(null);

  const fetchListOrderById = useCallback(async () => {
    try {
      const response = await axios.get(`${endpoints.order.get_order_by_id}/${orderId}`, {
        headers: {
          Authorization: `Bearer ${user?.accessToken}`,
          'Content-Type': 'application/json',
        },
      });
      SetInvoice(response?.data?.data);
    } catch (error) {
      console.error('Error fetching withdrawals:', error);
    }
  }, [orderId, user?.accessToken]); 

  useEffect(() => {
    fetchListOrderById();
  }, [fetchListOrderById]);

  // const [currentStatus, setCurrentStatus] = useState(invoice.statusReceived);

    const fullName = `${invoice?.firstName} ${invoice?.lastName}`;

    const [openQR, setOpenQR] = useState(false);

// Khi bạn muốn mở QR dialog
const handleOpenQRDialog = () => {
  setOpenQR(true);
};

// Khi bạn muốn đóng QR dialog
const handleCloseQRDialog = () => {
  setOpenQR(false);
};



  const renderTotal = (
    <>
      <StyledTableRow>
        <TableCell colSpan={3} />
        <TableCell sx={{ color: 'text.secondary' }}>
          <Box sx={{ mt: 2 }} />
          Subtotal
        </TableCell>
        <TableCell width={120} sx={{ typography: 'subtitle2' }}>
          <Box sx={{ mt: 2 }} />
          {fCurrency(invoice?.totalAmount)}
        </TableCell>
      </StyledTableRow>

      <StyledTableRow>
        <TableCell colSpan={3} />
        <TableCell sx={{ color: 'text.secondary' }}>
          <Box sx={{ mt: 0.5 }} />
          QR Code
        </TableCell>
        <TableCell width={120} sx={{ typography: 'subtitle2' }}>
          <Box sx={{ mt: 1 }} />
          <IconButton onClick={handleOpenQRDialog} >
          <QrCodeScannerIcon />
          </IconButton>
        </TableCell>
      </StyledTableRow>
    
      <StyledTableRow>
        <TableCell colSpan={3} />
        <TableCell sx={{ color: 'text.secondary' }}>Consignment</TableCell>
        <TableCell width={120} sx={{ color: 'error.main', typography: 'body2' }}>
          {invoice?.consignment}
        </TableCell>
      </StyledTableRow>

      <StyledTableRow>
        <TableCell colSpan={3} />
        <TableCell sx={{ color: 'text.secondary' }}>Discount</TableCell>
        <TableCell width={120} sx={{ color: 'error.main', typography: 'body2' }}>
          {/* {fCurrency(-invoice.discount)} */} {invoice?.discount}
        </TableCell>
      </StyledTableRow>

      {/* <StyledTableRow>
        <TableCell colSpan={3} />
        <TableCell sx={{ color: 'text.secondary' }}>Taxes</TableCell>
        <TableCell width={120}>{fCurrency(invoice.taxes)}</TableCell>
      </StyledTableRow> */}

      <StyledTableRow>
        <TableCell colSpan={3} />
        <TableCell sx={{ typography: 'subtitle1' }}>Total</TableCell>
        <TableCell width={140} sx={{ typography: 'subtitle1' }}>
          {fCurrency(invoice?.totalAmount)}
        </TableCell>
      </StyledTableRow>
    </>
  );

  const renderFooter = (
    <Grid container>
      <Grid xs={12} md={9} sx={{ py: 3 }}>
        <Typography variant="subtitle2">NOTES</Typography>

        <Typography variant="body2">
          We appreciate your business. Should you need us to add VAT or extra notes let us know!
        </Typography>
      </Grid>

      <Grid xs={12} md={3} sx={{ py: 3, textAlign: 'right' }}>
        <Typography variant="subtitle2">Have a Question?</Typography>

        <Typography variant="body2">blockchainserver0001@gmail.com</Typography>
      </Grid>
    </Grid>
  );

  const renderList = (
    <TableContainer sx={{ overflow: 'unset', mt: 5 }}>
      <Scrollbar>
        <Table sx={{ minWidth: 960 }}>
          <TableHead>
            <TableRow>
              <TableCell width={40}>#</TableCell>

              <TableCell sx={{ typography: 'subtitle2' }}>Description</TableCell>

              <TableCell>Quantity</TableCell>

              <TableCell align="right">Unit price</TableCell>

              <TableCell align="right">Total</TableCell>
            </TableRow>
          </TableHead>

          <TableBody>
            {invoice?.orderDetails?.map((row, index) => (
              <TableRow key={index}>
                <TableCell>{index + 1}</TableCell>

                <TableCell>
                  {/* <Box sx={{ maxWidth: 560 }}>
                    <Typography variant="subtitle2">{row?.product?.productName}</Typography>

                    <Typography variant="body2" sx={{ color: 'text.secondary' }} noWrap>
                      {row?.product?.description}
                    </Typography>
                  </Box> */}
                  <Box sx={{ display: 'flex', alignItems: 'center' }}>
                  <Box
                    component="img"
                    src={`/assets/images/${row?.product?.imgUrl}`}
                    alt={row?.product?.productName}
                    sx={{ width: 60, height: 60, borderRadius: '5%', mr: 2 }}
                  />

                  <Box sx={{ maxWidth: 560 }}>
                    <Typography variant="subtitle2" noWrap>
                      {row?.product?.productName}
                    </Typography>

                    <Typography variant="body2" sx={{ color: 'text.secondary' }} noWrap>
                      {row?.product?.description}
                    </Typography>
                  </Box>
                </Box>
                </TableCell>

                <TableCell>{row?.quantity}</TableCell>

                <TableCell align="right">{fCurrency(row?.price)}</TableCell>

                <TableCell align="right">{fCurrency(row.price * row.quantity)}</TableCell>
              </TableRow>
            ))}

            {renderTotal}
          </TableBody>
        </Table>
        <QRcodeView
        openQr={openQR}
        onCloseQr={handleCloseQRDialog}
        qrcode={invoice?.qrCode}
/>
      </Scrollbar>

    </TableContainer>
  );

  return (
    <>
      <InvoiceToolbar
        invoice={invoice}
        currentStatus={invoice?.statusReceived || ''}
        // onChangeStatus={handleChangeStatus}
        statusOptions={INVOICE_STATUS_OPTIONS}
      />

      <Card sx={{ pt: 5, px: 5 }}>
        <Box
          rowGap={5}
          display="grid"
          alignItems="center"
          gridTemplateColumns={{
            xs: 'repeat(1, 1fr)',
            sm: 'repeat(2, 1fr)',
          }}
        >
          <Box
            component="img"
            alt="logo"
            src="/logo/logo_single.svg"
            sx={{ width: 48, height: 48 }}
          />

          <Stack spacing={1} alignItems={{ xs: 'flex-start', md: 'flex-end' }}>
            <Label
              variant="soft"
              color={
                (invoice?.statusReceived === 'RECEIVED' && 'success') ||
                (invoice?.statusReceived === 'NOT_RECEIVED' && 'warning') ||
                (invoice?.statusReceived === 'UNVERIFIED' && 'error') ||
                'default'
              }
            >
              {invoice?.statusReceived}
            </Label>

            <Typography variant="h6">{invoice?.id}</Typography>
          </Stack>

          <Stack sx={{ typography: 'body2' }}>
            <Typography variant="subtitle2" sx={{ mb: 1 }}>
              Invoice From
            </Typography>
            BGSS Company
            <br />
            Ho Chi Minh City, VietNam
            <br />
            Phone: 0369050906
            <br />
          </Stack>

          <Stack sx={{ typography: 'body2' }}>
            <Typography variant="subtitle2" sx={{ mb: 1 }}>
              Invoice To
            </Typography>
            {fullName}
            <br />
            {invoice?.address}
            <br />
            Phone: {invoice?.phoneNumber}
            <br />
          </Stack>

          <Stack sx={{ typography: 'body2' }}>
            <Typography variant="subtitle2" sx={{ mb: 1 }}>
              Date Create
            </Typography>
            {fDate(invoice?.createDate)}
          </Stack>

          <Stack sx={{ typography: 'body2' }}>
            <Typography variant="subtitle2" sx={{ mb: 1 }}>
              Due Date
            </Typography>
            Unlimited
          </Stack>
        </Box>

        {renderList}

        <Divider sx={{ mt: 5, borderStyle: 'dashed' }} />

        {renderFooter}
      </Card>
    </>
  );
}

InvoiceDetails.propTypes = {
  // invoice: PropTypes.object,
  orderId: PropTypes.number,
};
