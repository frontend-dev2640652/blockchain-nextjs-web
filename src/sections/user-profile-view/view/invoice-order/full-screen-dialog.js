import { forwardRef } from 'react';
import PropTypes from 'prop-types';

import List from '@mui/material/List';
import Slide from '@mui/material/Slide';
import Button from '@mui/material/Button';
import AppBar from '@mui/material/AppBar';
import Dialog from '@mui/material/Dialog';
import Divider from '@mui/material/Divider';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import ListItemText from '@mui/material/ListItemText';
import ListItemButton from '@mui/material/ListItemButton';

import Iconify from 'src/components/iconify';
import scrollbar from 'src/components/scrollbar';

import InvoiceDetails from './invoice-details';

// import { useBoolean } from 'src/hooks/use-boolean';

// ----------------------------------------------------------------------

const Transition = forwardRef((props, ref) => <Slide direction="up" ref={ref} {...props} />);



export default function FullScreenDialog(props) {
  const { open, onClose, orderId, user } = props;

  return (
    <>
      {/* <Button variant="outlined" color="error" onClick={dialog.onTrue}>
        Full Screen Dialogs
      </Button> */}



      <Dialog fullScreen open={open} TransitionComponent={Transition} onClose={onClose}>
        <AppBar position="relative" color="default">
          <Toolbar>
            <IconButton color="inherit" edge="start" onClick={onClose}>
              <Iconify icon="mingcute:close-line" />
            </IconButton>

            <Typography variant="h6" sx={{ flex: 1, ml: 2 }}>
              Order Detail
            </Typography>
          </Toolbar>
        </AppBar>
        <scrollbar>
          <InvoiceDetails user={user} orderId={orderId}/>
        </scrollbar>
      </Dialog>
    </>
  );
}

FullScreenDialog.propTypes = {
  open: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  orderId: PropTypes.string.isRequired,
  user: PropTypes.object.isRequired,
};
