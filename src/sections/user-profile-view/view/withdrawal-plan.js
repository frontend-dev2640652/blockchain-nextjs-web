'use client';

import { useEffect, useState, useCallback } from 'react';

import Card from '@mui/material/Card';
import CancelIcon from '@mui/icons-material/Cancel';
import { Box, Stack, Popover, Typography } from '@mui/material';
import QrCodeScannerIcon from '@mui/icons-material/QrCodeScanner';
import {
  DataGrid,
  GridToolbarExport,
  GridActionsCellItem,
  GridToolbarContainer,
  GridToolbarQuickFilter,
  GridToolbarFilterButton,
  GridToolbarColumnsButton,
  GridToolbarDensitySelector,
} from '@mui/x-data-grid';

import { useMockedUser } from 'src/hooks/use-mocked-user';

import axios, { endpoints } from 'src/utils/axios';
import { fDate, fTime } from 'src/utils/format-time';

import Label from 'src/components/label/label';
import Iconify from 'src/components/iconify/iconify';
import EmptyContent from 'src/components/empty-content';
import FormProvider from 'src/components/hook-form/form-provider';

import QRcodeView from './verify-form/qrcode-view';
import ModernVerifyView from './verify-form/verification-form';
import CancelDialogView from './verify-form/confirm-dialog-view';

export default function WithDrawlPlan() {
  const { user } = useMockedUser();

  const [withdraws, setWithdraws] = useState([]);

  const [withdrawId, setWithdrawId] = useState(null);

  const [anchorEl, setAnchorEl] = useState(null);

  const [openDialog, setOpenDialog] = useState(false);

  const [openQRDialog, setOpenQRDialog] = useState(false);

  const [qrcode, setQrcode] = useState(null);

  const [openCancelDialog, setOpenCancelDialog] = useState(false);

  const handleCloseCancelDialog = () => {
    setOpenCancelDialog(false);
  };

  const handleCloseQRDialog = () => {
    setOpenQRDialog(false);
  };

  const handleCloseDialog = () => {
    setOpenDialog(false);
  };

  const handlePopoverClose = () => {
    setAnchorEl(null);
  };

  const handlePopoverOpen = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const open = Boolean(anchorEl);

  const handleClickVerify = (params) => {
    setWithdrawId(params.row.id);
    setOpenDialog(true);
  };

  const handleClickCancel = (params) => {
    setWithdrawId(params.row.id);
    setOpenCancelDialog(true);
  };

  const handleClickQRcode = (params) => {
    setQrcode(params.row.withdrawQrCode);
    setOpenQRDialog(true);
  };

  const fetchWithdraws = useCallback(async () => {
    try {
      const response = await axios.get(`${endpoints.withdrawal.withdrawals}/${user?.id}`, {
        headers: {
          Authorization: `Bearer ${user?.accessToken}`,
          'Content-Type': 'application/json',
        },
      });
      setWithdraws(response.data.data);
    } catch (error) {
      console.error('Error fetching withdrawals:', error);
    }
  }, [user?.id, user?.accessToken]); // Dependencies array

  // Gọi hàm fetchWithdraws khi component mounts và dependencies thay đổi.
  useEffect(() => {
    fetchWithdraws();
  }, [fetchWithdraws]);

  const cancel = useCallback(async () => {
    try {
      const response = await axios.get(`${endpoints.withdrawal.cancel}/${user?.id}`, {
        headers: {
          Authorization: `Bearer ${user?.accessToken}`,
          'Content-Type': 'application/json',
        },
      });
      setWithdraws(response.data.data);
    } catch (error) {
      console.error('Error fetching withdrawals:', error);
    }
  }, [user?.id, user?.accessToken]); // Dependencies array

  // Gọi hàm fetchWithdraws khi component mounts và dependencies thay đổi.
  useEffect(() => {
    cancel();
    fetchWithdraws();
  }, [cancel, fetchWithdraws]);

  const baseColumns = [
    { field: 'id', headerName: 'ID', width: 70 },
    {
      field: 'amount',
      headerName: 'Weight',
      headerAlign: 'center',
      align: 'center',
      type: 'number',
      width: 230,
      editable: false,
    },
    {
      field: 'goldUnit',
      headerName: 'Gold Unit',
      headerAlign: 'center',
      align: 'center',
      type: 'singleSelect',
      valueOptions: ['KILOGRAM', 'MACE', 'TAEL', 'GRAM', 'TROY_OZ'],
      width: 230,
      editable: false,
    },
    {
      field: 'transactionDate',
      headerName: 'Created At',
      headerAlign: 'center',
      align: 'center',
      type: 'dateTime',
      width: 230,
      valueGetter: (params) => (params.value ? new Date(params.value) : null),
      renderCell: (params) => `${fDate(params.value)} ${fTime(params.value)}`,
    },
    {
      field: 'status',
      headerName: 'Status',
      headerAlign: 'center',
      align: 'center',
      width: 250,
      renderCell: (params) => (
        <>
          <Label
            onClick={handlePopoverOpen}
            variant="soft"
            color={
              (params.value === 'UNVERIFIED' && 'error') ||
              (params.value === 'PENDING' && 'warning') ||
              (params.value === 'CONFIRMED' && 'primary') ||
              (params.value === 'COMPLETED' && 'success') ||
              (params.value === 'CANCELLED' && 'default')
            }
            sx={{ mx: 'auto' }}
          >
            {params.value}
          </Label>
          <Popover
            open={open}
            anchorEl={anchorEl}
            onClose={handlePopoverClose}
            anchorOrigin={{
              vertical: 'bottom',
              horizontal: 'center',
            }}
            transformOrigin={{
              vertical: 'top',
              horizontal: 'center',
            }}
            sx={{ '& .MuiPaper-root': { boxShadow: 'none' } }}
          >
            <Box sx={{ p: 2, maxWidth: 280 }}>
              <Typography variant="subtitle1" gutterBottom>
                Verification withdraw
              </Typography>
              <Typography variant="body2" sx={{ color: 'text.secondary' }}>
                Please attach the action to sent the OTP to verification!
              </Typography>
            </Box>
          </Popover>
        </>
      ),
    },

    {
      type: 'actions',
      field: 'actions',
      headerName: 'Actions',
      align: 'center',
      headerAlign: 'center',
      width: 100,
      sortable: false,
      filterable: false,
      disableColumnMenu: true,
    },
  ];

  const customizedColumns = baseColumns.map((col) => {
    if (col.type === 'actions') {
      return {
        ...col,
        getActions: (params) => [
          <GridActionsCellItem
            showInMenu
            icon={<Iconify icon="solar:pen-bold" />}
            label="Verify Withdraw"
            onClick={() => handleClickVerify(params)}
          />,
          <GridActionsCellItem
            showInMenu
            icon={<QrCodeScannerIcon />}
            label="QR Code"
            onClick={() => handleClickQRcode(params)}
          />,
          <GridActionsCellItem
            showInMenu
            icon={<CancelIcon />}
            label="Cancel withdraw"
            sx={{ color: 'error.main' }}
            onClick={() => handleClickCancel(params)}
          />,
        ],
      };
    }
    return col;
  });

  return (
    <FormProvider>
      <Stack component={Card} spacing={3} sx={{ p: 3 }}>
        <DataGrid
          disableRowSelectionOnClick
          rows={withdraws}
          columns={customizedColumns}
          slots={{
            toolbar: CustomToolbar,
            noRowsOverlay: () => <EmptyContent title="No Data" />,
            noResultsOverlay: () => <EmptyContent title="No results found" />,
          }}
          slotProps={{
            toolbar: {
              showQuickFilter: true,
            },
            // columnsPanel: {
            //   getTogglableColumns,
            // },
          }}
        />
        <ModernVerifyView
          open={openDialog}
          onClose={handleCloseDialog}
          withdrawId={withdrawId}
          user={user}
          onVerifySuccess={fetchWithdraws}
        />
        <QRcodeView openQr={openQRDialog} onCloseQr={handleCloseQRDialog} qrcode={qrcode} />
        <CancelDialogView
          openCancel={openCancelDialog}
          onClose={handleCloseCancelDialog}
          withdrawId={withdrawId}
          user={user}
          onVerifySuccess={fetchWithdraws}
        />
      </Stack>
    </FormProvider>
  );
}

function CustomToolbar() {
  return (
    <GridToolbarContainer>
      <GridToolbarQuickFilter />
      <Box sx={{ flexGrow: 1 }} />
      <GridToolbarColumnsButton />
      <GridToolbarFilterButton />
      <GridToolbarDensitySelector />
      <GridToolbarExport />
    </GridToolbarContainer>
  );
}
