'use-client';

import axios, { endpoints } from 'src/utils/axios';

export async function getSecretKey(userId) {
  try {
    const response = await axios.post(`${endpoints.secretkey.key}/${userId}`);
    const { data } = response;
    return data;
  } catch (error) {
    console.error('Error fetching secret key:', error);
    throw error;
  }
}
