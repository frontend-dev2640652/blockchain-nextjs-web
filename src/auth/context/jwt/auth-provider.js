'use client';

import PropTypes from 'prop-types';
import { useMemo, useEffect, useReducer, useCallback, useContext } from 'react';

import axios, { endpoints } from 'src/utils/axios';

import { AuthContext } from './auth-context';
import { setSession, isValidToken } from './utils';

// ----------------------------------------------------------------------
/**
 * NOTE:
 * We only build demo at basic level.
 * Customer will need to do some extra handling yourself if you want to extend the logic and other features...
 */
// ----------------------------------------------------------------------

export const useAuth = () => useContext(AuthContext);

const initialState = {
  user: null,
  loading: true,
};

const reducer = (state, action) => {
  if (action.type === 'INITIAL') {
    return {
      loading: false,
      user: action.payload.user,
    };
  }
  if (action.type === 'LOGIN') {
    return {
      ...state,
      user: action.payload.user,
    };
  }
  // if (action.type === 'REGISTER') {
  //   return {
  //     ...state,
  //     signup: action.payload.signup,
  //   };
  // }
  if (action.type === 'LOGOUT') {
    return {
      ...state,
      user: null,
    };
  }
  return state;
};

// ----------------------------------------------------------------------

export function AuthProvider({ children }) {
  const [state, dispatch] = useReducer(reducer, initialState);

  const STORAGE_KEY = 'accessToken';
  const USER_ID_KEY = 'id';

  const initialize = useCallback(async () => {
    try {
      const accessToken = sessionStorage.getItem(STORAGE_KEY);
      const userId = sessionStorage.getItem(USER_ID_KEY);

      if (accessToken && isValidToken(accessToken) && userId) {
        setSession(accessToken);
        setSession(userId);
        const url = `${endpoints.auth.showUserInfo}/${userId}`;
        // const response = await axios.get(endpoints.auth.me);
        const response = await axios.get(url);

        const { user } = response.data;

        dispatch({
          type: 'INITIAL',
          payload: {
            user: {
              ...user,
              accessToken,
              userId,
            },
          },
        });
      } else {
        dispatch({
          type: 'INITIAL',
          payload: {
            user: null,
          },
        });
      }
    } catch (error) {
      console.error(error);
      dispatch({
        type: 'INITIAL',
        payload: {
          user: null,
        },
      });
    }
  }, []);

  useEffect(() => {
    initialize();
  }, [initialize]);

  // LOGIN
  const login = useCallback(async (username, password) => {
    const data = {
      username,
      password,
    };

    const response = await axios.post(endpoints.auth.login, data);

    const { accessToken, id, username: responseUsername, email, roles, tokenType } = response.data;

    sessionStorage.setItem(STORAGE_KEY, accessToken);
    sessionStorage.setItem(USER_ID_KEY, id);

    setSession(accessToken);

    const urlUserInfo = `${endpoints.auth.showUserInfo}/${id}`;
    const responseUserInfo = await axios.get(urlUserInfo);
    const userInfoFetch = responseUserInfo.data.data;

    dispatch({
      type: 'LOGIN',
      payload: {
        user: {
          id,
          username: responseUsername,
          email,
          roles,
          accessToken,
          tokenType,
          balance: userInfoFetch.userInfo.balance.amount,
          userInfoFetch,
        },
      },
    });
  }, []);

  // REGISTER
  const register = useCallback(
    async (username, email, phoneNumber, password, firstName, lastName, address) => {
      const dataSignup = {
        username,
        email,
        phoneNumber,
        password,
        firstName,
        lastName,
        address,
      };

      const response = await axios.post(endpoints.auth.register, dataSignup);

      // const { accessToken, user } = response.data;

      // sessionStorage.setItem(STORAGE_KEY, accessToken);

      // dispatch({
      //   type: 'REGISTER',
      //   payload: {
      //     signup: {
      //       // ...user,
      //       // accessToken,
      //     ...response?.data,
      //     },
      //   },
      // });
      console.log(response.data);
      return response?.data;
    },
    []
  );

  // LOGOUT
  const logout = useCallback(async () => {
    setSession(null);
    dispatch({
      type: 'LOGOUT',
    });
  }, []);

  const fetchUserInfo = useCallback(async (accessToken, userId) => {
    if (accessToken && isValidToken(accessToken) && userId) {
      try {
        const response = await axios.get(`${endpoints.auth.showUserInfo}/${userId}`, {
          headers: {
            Authorization: `Bearer ${accessToken}`,
            'Content-Type': 'application/json',
          },
        });
        const userInfo = response.data?.data;
        dispatch({ type: 'LOGIN', payload: { user: userInfo } });
      } catch (error) {
        console.error('Failed to fetch user info:', error);
      }
    }
  }, []);

  useEffect(() => {}, [fetchUserInfo]);

  // ----------------------------------------------------------------------

  const checkAuthenticated = state.user ? 'authenticated' : 'unauthenticated';

  const status = state.loading ? 'loading' : checkAuthenticated;

  const memoizedValue = useMemo(
    () => ({
      user: state.user,
      method: 'jwt',
      loading: status === 'loading',
      authenticated: status === 'authenticated',
      unauthenticated: status === 'unauthenticated',
      //
      login,
      register,
      logout,
      initialize,
      fetchUserInfo,
    }),
    [state, status, login, register, logout, initialize, fetchUserInfo]
  );

  return <AuthContext.Provider value={memoizedValue}>{children}</AuthContext.Provider>;
}

AuthProvider.propTypes = {
  children: PropTypes.node,
};
