import { useState } from 'react';

import Box from '@mui/material/Box';
import Link from '@mui/material/Link';
import Stack from '@mui/material/Stack';
import Alert from '@mui/material/Alert';
import { Snackbar } from '@mui/material';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import { useTheme } from '@mui/material/styles';
import Container from '@mui/material/Container';
import Typography from '@mui/material/Typography';
import Badge, { badgeClasses } from '@mui/material/Badge';
import MonetizationOnIcon from '@mui/icons-material/MonetizationOn';
import SavingsRoundedIcon from '@mui/icons-material/SavingsRounded';

import { paths } from 'src/routes/paths';

import { useOffSetTop } from 'src/hooks/use-off-set-top';
import { useResponsive } from 'src/hooks/use-responsive';
import { useMockedUser } from 'src/hooks/use-mocked-user';
import { useMockedUserInfo } from 'src/hooks/useMockUserInfo';

import axios, { endpoints } from 'src/utils/axios';

import { bgBlur } from 'src/theme/css';
import { useAuthContext } from 'src/auth/hooks';
import { useAuth } from 'src/auth/context/jwt/auth-provider';

import Logo from 'src/components/logo';

import NavMobile from './nav/mobile';
import NavDesktop from './nav/desktop';
import { HEADER } from '../config-layout';
import { navConfig } from './config-navigation';
import LoginButton from '../common/login-button';
import HeaderShadow from '../common/header-shadow';
import SettingsButton from '../common/settings-button';
import AccountPopover from '../common/account-popover';
import FormDialog from './dialog-form-withdraw/form-dialog';

// import LanguagePopover from '../common/language-popover';

// ----------------------------------------------------------------------

export default function Header() {
  const theme = useTheme();

  const mdUp = useResponsive('up', 'md');

  const { user } = useAuth();

  const { userInfoFetch } = useMockedUserInfo();

  // console.log(userInfoFetch);

  const offsetTop = useOffSetTop(HEADER.H_DESKTOP);

  const [open, setOpen] = useState(false);

  const [weight, setWeightToWithdraw] = useState('');

  const [openSnackbar, setOpenSnackbar] = useState(false);
  const [snackbarMessage, setSnackbarMessage] = useState('');
  const [snackbarSeverity, setSnackbarSeverity] = useState('info');

  const showSnackbar = (message, severity) => {
    setSnackbarMessage(message);
    setSnackbarSeverity(severity);
    setOpenSnackbar(true);
  };

  const handleOpenDialog = () => {
    setOpen(true);
  };

  const handleCloseDialog = () => {
    setOpen(false);
  };

  async function requestWithdrawGold(weightToWithdraw, unit) {
    try {
      const response = await axios.post(
        `${endpoints.withdrawal.request}/${user?.id}?weightToWithdraw=${weightToWithdraw}&unit=${unit}`,
        {
          headers: {
            Authorization: `Bearer ${user?.accessToken}`,
            'Content-Type': 'application/json',
          },
        }
      );
        // Đặt một timeout để hiển thị Snackbar thứ hai sau 1000 ms (1 giây)
      showSnackbar('Success! Please check your email!', 'success');
      setTimeout(() => {
      showSnackbar('Access Withdraw Page To verify!', 'info');
    }, 3500);
      return response.data;
    } catch (error) {
      showSnackbar('Failed to request withdrawal of gold.', 'error');
      return null;
    }
  }

  const handleSubmitDialog = async (weightToWithdraw, unit) => {
    try {
      const data = await requestWithdrawGold(weightToWithdraw, unit);
      if (data) {
        handleCloseDialog();
        setWeightToWithdraw('');
      }
    } catch (error) {
      console.error('Error when making withdraw request:', error);
    }
  };

  const handleCloseSnackbar = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnackbar(false);
  };

  return (
    <>
      <AppBar>
        <Toolbar
          disableGutters
          sx={{
            height: {
              xs: HEADER.H_MOBILE,
              md: HEADER.H_DESKTOP,
            },
            transition: theme.transitions.create(['height'], {
              easing: theme.transitions.easing.easeInOut,
              duration: theme.transitions.duration.shorter,
            }),
            ...(offsetTop && {
              ...bgBlur({
                color: theme.palette.background.default,
              }),
              height: {
                md: HEADER.H_DESKTOP_OFFSET,
              },
            }),
          }}
        >
          <Container sx={{ height: 1, display: 'flex', alignItems: 'center' }}>
            <Badge
              sx={{
                [`& .${badgeClasses.badge}`]: {
                  top: 8,
                  right: -16,
                },
              }}
              badgeContent={
                <Link
                  href={paths.changelog}
                  target="_blank"
                  rel="noopener"
                  underline="none"
                  sx={{ ml: 1 }}
                >
                  {/* <Label color="info" sx={{ textTransform: 'unset', height: 22, px: 0.5 }}>
                  v5.7.0
                </Label> */}
                </Link>
              }
            >
              <Logo />
            </Badge>

            <Box sx={{ flexGrow: 1 }} />

            {mdUp && <NavDesktop data={navConfig} />}

            <Stack alignItems="center" direction={{ xs: 'row', md: 'row-reverse' }}>
              {/* <Button variant="contained" target="_blank" rel="noopener" href={paths.minimalUI}>
              Purchase Now
            </Button> */}

              {mdUp && (user ? <AccountPopover user={user} /> : <LoginButton />)}

              <SettingsButton
                sx={{
                  ml: { xs: 1, md: 0 },
                  mr: { md: 2 },
                }}
              />

              {!mdUp && <NavMobile data={navConfig} />}
            </Stack>
            {user && (
              <Stack direction="row" alignItems="center">
                <MonetizationOnIcon
                  sx={{ color: 'gold', backgroundColor: 'yellow', borderRadius: '50%' }}
                />
                <Typography>{user?.userInfoFetch?.userInfo?.balance?.amount}</Typography>
                <SavingsRoundedIcon
                  onClick={handleOpenDialog}
                  sx={{ color: 'gold', backgroundColor: 'yellow', borderRadius: '50%' }}
                />
                <Typography>{user?.userInfoFetch?.userInfo?.inventory?.totalWeightOz}</Typography>
              </Stack>
            )}
          </Container>
        </Toolbar>

        {offsetTop && <HeaderShadow />}
      </AppBar>
      <FormDialog
        open={open}
        onClose={handleCloseDialog}
        weightToWithdraw={weight}
        setweightToWithdraw={setWeightToWithdraw}
        onSubmit={handleSubmitDialog}
      />
      <Snackbar open={openSnackbar} autoHideDuration={6000} onClose={handleCloseSnackbar}>
        <Alert onClose={handleCloseSnackbar} severity={snackbarSeverity} sx={{ width: '100%' }}>
          {snackbarMessage}
        </Alert>
      </Snackbar>
    </>
  );
}
