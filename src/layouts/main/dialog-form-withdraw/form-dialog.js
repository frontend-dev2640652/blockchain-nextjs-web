import PropTypes from 'prop-types';
import { useState, useEffect, useCallback } from 'react';

import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import { Autocomplete } from '@mui/material';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';
import LoadingButton from '@mui/lab/LoadingButton';
import DialogTitle from '@mui/material/DialogTitle';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
// ----------------------------------------------------------------------

FormDialog.propTypes = {
  open: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  weightToWithdraw: PropTypes.string.isRequired,
  setweightToWithdraw: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
};

export default function FormDialog({
  open,
  onClose,
  weightToWithdraw,
  setweightToWithdraw,
  onSubmit,
}) {
  const [unit, setUnit] = useState({ title: '' });

  const [isLoading, setIsLoading] = useState(false);
 
  const handleUnitChange = (event, newValue) => {
    setUnit(newValue || 'GRAM');
  };

  const handleSubmit = async () => {
    setIsLoading(true);
    try {
      await onSubmit(weightToWithdraw, unit.title);
    } finally {
      setIsLoading(false);
    }
    onClose();
  };


  return (
    <Dialog open={open} onClose={onClose}>
      <DialogTitle>Request Withdraw Gold</DialogTitle>

      <DialogContent>
        <Typography sx={{ mb: 3 }}>Process to withdraw your gold in Inventory</Typography>

        <TextField
          autoFocus
          fullWidth
          sx={{ mb: 3 }}
          type="number"
          margin="dense"
          variant="outlined"
          label="Weight"
          value={weightToWithdraw}
          onChange={(event) => setweightToWithdraw(event.target.value)}
        />
        <Autocomplete
          options={units}
          getOptionLabel={(option) => option.title}
          value={unit}
          onChange={handleUnitChange}
          sx={{ flex: 0.8 }}
          renderInput={(params) => (
            <TextField
              {...params}
              label="Gold Units"
              margin="none"
              InputProps={{
                ...params.InputProps,
                readOnly: true,
                style: {
                  backgroundColor: 'rgba(0, 0, 0, 0.09)',
                },
              }}
            />
          )}
          renderOption={(props, option) => (
            <li {...props} key={option.title}>
              {option.title}
            </li>
          )}
        />
      </DialogContent>

      <DialogActions>
        <Button onClick={onClose} variant="outlined" color="inherit">
          Cancel
        </Button>
        <LoadingButton onClick={handleSubmit} variant="contained" loading={isLoading}>
          Request!
        </LoadingButton>
      </DialogActions>
    </Dialog>
  );
}

const units = [
  { title: 'KILOGRAM' },
  { title: 'MACE' },
  { title: 'TAEL' },
  { title: 'TROY_OZ' },
];
