import useSWR from 'swr';
import { useMemo } from 'react';

import { fetcher, endpoints } from 'src/utils/axios';

export function useGetRate() {
  const URL = endpoints.rate.list;

  const { data, isLoading, error, isValidating } = useSWR(URL, fetcher);

  const memoizedValue = useMemo(
    () => ({
      rates: data?.data || [],
      ratesLoading: isLoading,
      ratesError: error,
      ratesValidating: isValidating,
      ratesEmpty: !isLoading && !data?.data.length,
    }),
    [data?.data, error, isLoading, isValidating]
  );

  return memoizedValue;
}
// ---------------------------------------------------------------------
