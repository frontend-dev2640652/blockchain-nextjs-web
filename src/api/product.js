import useSWR from 'swr';
import { useMemo } from 'react';

import { fetcher, endpoints } from 'src/utils/axios';

// ----------------------------------------------------------------------

export function useGetProducts() {
  const URL = endpoints.product.list;

  const { data, error, isValidating } = useSWR(URL, fetcher);
  const isLoading = !data && !error;

  const memoizedValue = useMemo(
    () => ({
      products: data?.data || [],
      productsLoading: isLoading,
      productsError: error,
      productsValidating: isValidating,
      productsEmpty: !isLoading && data?.data?.length === 0,
    }),
    [data, error, isLoading, isValidating]
  );

  return memoizedValue;
}

// ----------------------------------------------------------------------

export function useGetProduct(id) {
  const URL = id ? `${endpoints.product.details}/${id}` : '';

  const { data, isLoading, error, isValidating } = useSWR(URL, fetcher);
  console.log(data, isLoading, error);
  const memoizedValue = useMemo(
    () => ({
      product: data?.data,
      productLoading: isLoading,
      productError: error,
      productValidating: isValidating,
    }),
    [data, error, isLoading, isValidating]
  );

  return memoizedValue;
}

// ----------------------------------------------------------------------

// export function useSearchProducts(search) {
//   const URL = search ? [endpoints.product.search, { params: { search } }] : '';
//
//   const { data, isLoading, error, isValidating } = useSWR(URL, fetcher, {
//     keepPreviousData: true,
//   });
//
//   const memoizedValue = useMemo(
//     () => ({
//       searchResults: data?.data || [],
//       searchLoading: isLoading,
//       searchError: error,
//       searchValidating: isValidating,
//       searchEmpty: !isLoading && (data?.products?.length > 0),
//     }),
//     [data, error, isLoading, isValidating]
//   );
//
//   return memoizedValue;
// }

export function useSearchProducts(search) {
  const URL = search ? `${endpoints.product.search}?search=${search}` : '';

  const { data, isLoading, error, isValidating } = useSWR(URL, fetcher, {
    keepPreviousData: true,
  });

  const memoizedValue = useMemo(() => {
    // Chuyển đổi dữ liệu nhận được từ API
    const searchResults =
      data?.data.map((product) => ({
        id: product.id,
        name: product.productName,
        img: product.imgUrl,
      })) || [];

    return {
      searchResults,
      searchLoading: isLoading,
      searchError: error,
      searchValidating: isValidating,
      searchEmpty: !isLoading && searchResults.length === 0, // Logic này để kiểm tra nếu không có kết quả tìm kiếm
    };
  }, [data, error, isLoading, isValidating]);

  return memoizedValue;
}
