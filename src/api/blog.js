import useSWR from 'swr';
import { useMemo } from 'react';

import { fetcher, endpoints } from 'src/utils/axios';

// ----------------------------------------------------------------------

export function useGetPosts() {
  const URL = endpoints.post.list;

  const { data, isLoading, error, isValidating } = useSWR(URL, fetcher);

  const memoizedValue = useMemo(
    () => ({
      posts: data?.data || [],
      postsLoading: isLoading,
      postsError: error,
      postsValidating: isValidating,
      postsEmpty: !isLoading && !data?.data.length,
    }),
    [data?.data, error, isLoading, isValidating]
  );

  return memoizedValue;
}

// ----------------------------------------------------------------------

export function useGetPost(id) {
  const URL = id ? `${endpoints.post.details}/${id}` : '';
  const { data, isLoading, error, isValidating } = useSWR(URL, fetcher);
  if (data) {
    data.data.content = formatPostHTMLWithDiscount(data.data.content);
  }
  const memoizedValue = useMemo(
    () => ({
      post: data?.data || [],
      postLoading: isLoading,
      postError: error,
      postValidating: isValidating,
    }),
    [data?.data, error, isLoading, isValidating] // Bổ sung id vào mảng dependency
  );
  return memoizedValue;
}

// ----------------------------------------------------------------------

export function useGetComment(postId) {
  const URL = postId ? `${endpoints.rate.list}?postId=${postId}&showHiding=false` : '';
  // const URL = `https://www.bgss-company.tech/api/auth/show-all-rate?postId=${postId}&showHiding=false`;
  const { data, isLoading, error, isValidating } = useSWR(URL, fetcher);

  const memoizedValue = useMemo(
    () => ({
      comments: data?.data || [],
      commentsLoading: isLoading,
      commentsError: error,
      commentsValidating: isValidating,
      commentsEmpty: !isLoading && !data?.data.length,
    }),
    [data?.data, error, isLoading, isValidating]
  );

  return memoizedValue;
}
//-----------------------------------------------------------------------
export function useGetLatestPosts(title) {
  const URL = title ? [endpoints.post.latest, { params: { title } }] : '';

  const { data, isLoading, error, isValidating } = useSWR(URL, fetcher);

  const memoizedValue = useMemo(
    () => ({
      latestPosts: data?.latestPosts || [],
      latestPostsLoading: isLoading,
      latestPostsError: error,
      latestPostsValidating: isValidating,
      latestPostsEmpty: !isLoading && !data?.latestPosts.length,
    }),
    [data?.latestPosts, error, isLoading, isValidating]
  );

  return memoizedValue;
}

// ----------------------------------------------------------------------

export function useSearchPosts(query) {
  const URL = query ? [endpoints.post.search, { params: { query } }] : '';

  const { data, isLoading, error, isValidating } = useSWR(URL, fetcher, {
    keepPreviousData: true,
  });

  const memoizedValue = useMemo(
    () => ({
      searchResults: data?.results || [],
      searchLoading: isLoading,
      searchError: error,
      searchValidating: isValidating,
      searchEmpty: !isLoading && !data?.results.length,
    }),
    [data?.results, error, isLoading, isValidating]
  );

  return memoizedValue;
}
const DISCOUNT_REGEX = /##DISCOUNT\((\w+?)\)/g; // ##DISCOUNT(Your_Code) // lay cai nay4
// voi ham nay
// const formatPostHTMLWithDiscount = (html) => {
//   function replacement() {
//     return `<span class="allow-discount-copy" data-value="$1"><span class="user-select-all">$1<span class="icon clickable"><i class="fas fa-copy"></i></span></span></span>`;
//     // phan nay ong sua lai html 1 ti de cho no giong template nha
//   }
//   return html.replace(DISCOUNT_REGEX, replacement());
// };
const formatPostHTMLWithDiscount = (html) => {
  function replacement(match, p1) {
    return `<span class="allow-discount-copy" data-value="${p1}">
              <span class="user-select-all">${p1}
                <CopyToClipboard text="${p1}" onCopy={() => alert('Copied to clipboard')}>
                  <button class="icon clickable copy-button">
                    <i class="fas fa-copy" style="font-size: 20px; transform: translateY(-1px);"></i>
                  </button>
                </CopyToClipboard>
              </span>
            </span>`;
    // phan nay ong sua lai html 1 ti de cho no giong template nha
  }

  return html.replace(DISCOUNT_REGEX, replacement);
};
