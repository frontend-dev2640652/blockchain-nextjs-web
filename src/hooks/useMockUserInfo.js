import { useState, useEffect, useCallback } from 'react';

import axios, { endpoints } from 'src/utils/axios';

import { useMockedUser } from './use-mocked-user';

export function useMockedUserInfo() {
  const { user } = useMockedUser();

  const [userInfoFetch, setUserInfoFetch] = useState(null);

  // Sử dụng useCallback để tránh tạo mới hàm mỗi lần component rerender
  const fetchUserInfo = useCallback(async () => {
    if (user?.accessToken && user.id) {
      try {
        const response = await axios.get(`${endpoints.auth.showUserInfo}/${user?.id}`, {
          headers: {
            Authorization: `Bearer ${user?.accessToken}`,
            'Content-Type': 'application/json',
          },
        });
        setUserInfoFetch(response.data?.data);
      } catch (error) {
        console.error('Failed to fetch user info:', error);
        setUserInfoFetch(null);
      }
    }
  }, [user?.id, user?.accessToken]);

  useEffect(() => {
    fetchUserInfo();
  }, [fetchUserInfo]);

  return { userInfoFetch, fetchUserInfo };
}
