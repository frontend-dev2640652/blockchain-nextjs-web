import GoldDashBoardView from 'src/sections/gold-dasboard/gold-dashboard-view';

export const metadata = {
  title: 'Gold Dashboard: Gold Dashboard',
};

export default function GoldDashBoardPage() {
  return <GoldDashBoardView />;
}
