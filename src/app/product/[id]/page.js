import PropTypes from 'prop-types';

import axios, { endpoints } from 'src/utils/axios';

import { ProductShopDetailsView } from 'src/sections/product/view';

// ----------------------------------------------------------------------

export const metadata = {
  title: 'Product: Details',
};

export default function ProductShopDetailsPage({ params }) {
  const { id } = params;

  return <ProductShopDetailsView id={id} />;
}

export async function generateStaticParams() {
  const res = await axios.get(endpoints.product.list);
  // console.log(JSON.stringify(res.data.data, null, 2));
  const returnValue =
    res.data.data?.map((data) => ({
      id: data.id.toString(),
    })) || [];
  // console.log(JSON.stringify(returnValue, null, 2));
  return returnValue;
}

ProductShopDetailsPage.propTypes = {
  params: PropTypes.shape({
    id: PropTypes.string,
  }),
};
