import AccountViewUser from 'src/sections/user-profile-view/view/user-account-view';

export const metadata = {
  title: 'Account view: User Info',
};

export default function AccountViewUserPage() {
  return <AccountViewUser />;
}
