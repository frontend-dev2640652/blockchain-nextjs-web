import axios from 'axios';

import { HOST_API } from 'src/config-global';

// ----------------------------------------------------------------------

const axiosInstance = axios.create({ baseURL: HOST_API });

axiosInstance.interceptors.response.use(
  (res) => res,
  (error) => Promise.reject((error.response && error.response.data) || 'Something went wrong')
);

export default axiosInstance;

// ----------------------------------------------------------------------

export const fetcher = async (args) => {
  const [url, config] = Array.isArray(args) ? args : [args];

  const res = await axiosInstance.get(url, { ...config });

  return res.data;
};

// ----------------------------------------------------------------------

export const endpoints = {
  chat: '/api/chat',
  kanban: '/api/kanban',
  calendar: '/api/calendar',
  auth: {
    // me: '/api/auth/me',
    showUserInfo: '/api/auth/show-user-info',
    login: '/api/auth/sign-in-v2',
    register: '/api/auth/sign-up',
  },
  users: {
    update: '/api/auth/update-user-info',
    avatar: '/api/auth/update-avatar',
    verifycicard: '/api/auth/verify/ciCard/image-verify',
  },
  mail: {
    list: '/api/mail/list',
    details: '/api/mail/details',
    labels: '/api/mail/labels',
  },
  rate: {
    create: '/api/auth//create-rate',
    details: '/api/mail/details',
    list: '/api/auth/show-all-rate',
    get: '/api/auth/get-rate-by-id',
    update: '/api/auth/update-rate/rateId=',
  },
  
  post: {
    list: '/api/auth/get-all-post',
    details: '/api/auth/get-post-by-id',
  },
  product: {
    list: '/api/auth/product/show-list-product',
    details: '/api/auth/product/get-product-by-id',
    search: '/api/auth/product/show-list-product',
    add_to_cart: '/api/auth/add-product-to-cart',
    list_cart: '/api/auth/get-list-cart-fellow-userid',
    update_quantity:  `/api/auth/update-quantity`,
    remove_cart: `/api/auth/remove-product-from-cart`,
  },
  order: {
    create: 'api/auth/create-order',
    verify: 'api/auth/verification-order',
    resent: 'api/auth/resent-otp',
    list: '/api/auth/get-order-list',
    get_order_by_id: 'api/auth/get-order-by-id',
  },
  transactions: {
    actions: '/api/auth/transactions',
    list: '/api/auth/user-transaction-list',
    convert: 'api/auth/convert/price-per-ounce',
    accepted: '/api/auth/transactions-accepted/web',
    socket: '/api/auth/ws',
  },
  secretkey: {
    key: '/api/auth/show-secret-key',
  },
  withdrawal: {
    request: '/api/auth/request-withdraw-gold',
    withdrawals: 'api/auth/withdraw-list-userinfo',
    verify: 'api/auth/request-withdraw-gold/verify',
    resent: 'api/auth/resent-otp-withdraw',
    cancel: 'api/auth/cancel',
  },
  metalpriceapi: {
    timeframe: '/api/auth/gold/timeframe',
  },
  socket: {
    forex: '/api/auth/ws',
    subcribe_forex: '', 
  },
  admin: {
    list_users: '/api/auth/show-list-user',
  },
  signature: 'api/auth/',
};
